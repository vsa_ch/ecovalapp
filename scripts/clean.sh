#!/bin/bash

START_DIR=$(pwd)
ROOT_DIR=$(cd "$(dirname $0)/.."; pwd) # OS X and BSD compatible abs. path

cleanup() {
    cd ${START_DIR}
}
abort() {
    echo "ERROR"
    exit 1
}

trap 'cleanup' 0
trap 'abort' ERR

cd ${ROOT_DIR}/src/app/www/
rm -rf *.pdf *.csv *.dat *.html $(ls *.txt | grep -v "README")
