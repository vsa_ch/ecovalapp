#!/bin/bash

START_DIR=$(pwd)
ROOT_DIR=$(cd "$(dirname $0)/.."; pwd) # OS X and BSD compatible abs. path

cleanup() {
    cd ${START_DIR}
}
abort() {
    echo "ERROR"
    exit 1
}

trap 'cleanup' 0
trap 'abort' ERR

dylib_path_find="/Library/Frameworks/R.framework"
dylib_path_target="@executable_path/../../lib"

cd ${ROOT_DIR}/src/app/R-Portable-Mac
for f in bin/exec/R $(ls -1 lib/*.dylib); do

    echo "re-linking dylibs in: ${ROOT_DIR}/src/app/R-Portable-Mac/${f}"
    codesign --remove-signature ${f}
    otool -L ${f} | grep "${dylib_path_find}" | grep -v $(echo ${f} | cut -f1 -d.) | while read line ; do
        dep_dylib=$(echo ${line// /} | cut -f1 -d"(")
        dep_dylib_target=${dylib_path_target}/$(basename ${dep_dylib})
        echo "    ${dep_dylib} => ${dep_dylib_target}"
        install_name_tool -change ${dep_dylib} ${dep_dylib_target} ${f}
    done
    echo

done
