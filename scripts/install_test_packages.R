# install test dependencies
cran_packages <- c(
    "diffviewer",
    "dplyr",
    "pdftools",
    "testthat",
    "tibble"
)
cran_repo <- "https://stat.ethz.ch/CRAN/"
outdated_packages <- rownames(old.packages(repos = cran_repo))
for (pkg.name in cran_packages) {
  if (!requireNamespace(pkg.name, quietly = TRUE) || pkg.name %in% outdated_packages) {
    install.packages(pkg.name, repos = cran_repo)
  }
}
