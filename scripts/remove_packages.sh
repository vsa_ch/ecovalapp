
#!/bin/bash


help() {
    echo "Usage:"
    echo "    ${0} OS_TYPE"
    echo ""
    echo "where:"
    echo "     OS_TYPE: \"win\" or \"mac\""
}

if [ $# != 1 ]; then
    help
    exit 1
fi

# more flexible: just grab first 3 chars and lowercase them; allows Windows, macOS etc.
OS_TYPE=$(echo ${1:0:3} | tr "[A-Z]" "[a-z]")
case ${OS_TYPE} in
    "mac")
        ;;
    "win")
        ;;
    *)
        help
        exit 1
        ;;
esac


START_DIR=$(pwd)
ROOT_DIR=$(cd "$(dirname $0)/.."; pwd) # OS X and BSD compatible abs. path
APP_SRC_DIR=${ROOT_DIR}/src/app
R_PORTABLE_REL_DIR=R-Portable-$(echo ${OS_TYPE:0:1} | tr "[a-z]" "[A-Z]")${OS_TYPE:1}

cleanup() {
    cd ${START_DIR}
}
abort() {
    echo "ERROR"
    exit 1
}

trap 'cleanup' 0
trap 'abort' ERR

# Built-in pkgs list from R Portable 4.1.0
R_PORTABLE_PKGS="base boot class cluster codetools compiler datasets foreign graphics grDevices grid KernSmooth lattice MASS Matrix methods mgcv nlme nnet parallel rpart spatial splines stats stats4 survival tcltk tools translations utils"

R_LIB_DIR="${APP_SRC_DIR}/${R_PORTABLE_REL_DIR}/library"
echo "Removing extra R packages (${OS_TYPE}).."
cd ${R_LIB_DIR}
echo $(pwd)
echo
for lib in $(ls -1 .); do
    if [[ ${R_PORTABLE_PKGS} =~ (^| )${lib}($| ) ]]; then
        echo " keeping: ${lib}"
    else
        echo "removing: ${lib}"
        rm -rf ${lib}
    fi
done
echo
