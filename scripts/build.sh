#!/bin/bash


help() {
    echo "Usage:"
    echo "    ${0} OS_TYPE"
    echo ""
    echo "where:"
    echo "     OS_TYPE: \"win\" or \"mac\""
}

if [ $# != 1 ]; then
    help
    exit 1
fi

# more flexible: just grab first 3 chars and lowercase them; allows Windows, macOS etc.
OS_TYPE=$(echo ${1:0:3} | tr "[A-Z]" "[a-z]")
case ${OS_TYPE} in
    "mac")
        # R_BIN="R"
        DIST_REL_DIR="EcovalApp-darwin-x64"
        APP_DIST_REL_DIR="${DIST_REL_DIR}/EcovalApp.app/Contents/Resources/app"
        ;;
    "win")
        # R_BIN="R.exe"
        DIST_REL_DIR="EcovalApp-win32-ia32"
        APP_DIST_REL_DIR="${DIST_REL_DIR}/resources/app"
        ;;
    *)
        help
        exit 1
        ;;
esac


START_DIR=$(pwd)
ROOT_DIR=$(cd "$(dirname $0)/.."; pwd) # OS X and BSD compatible abs. path
APP_SRC_DIR=${ROOT_DIR}/src/app
# Note: 1 lvl nesting for app/ as in src/ to keep relative refs for target
ROOT_BUILD_DIR=${ROOT_DIR}/build-${OS_TYPE}-$(date +"%y%m%d_%H%M%S")
APP_BUILD_DIR=${ROOT_BUILD_DIR}/app
R_PORTABLE_REL_DIR=R-Portable-$(echo ${OS_TYPE:0:1} | tr "[a-z]" "[A-Z]")${OS_TYPE:1}
# Note: do not add trailing / to directories, unless you want to copy out their contents
APP_RESOURCES="${R_PORTABLE_REL_DIR} R utils www config.yml *.R eawag.* LICENSE *.js package*.json"
# R_SCRIPT_REL="bin/${R_BIN} --vanilla --silent -f"
# R_INSTALL_PKGS_SCRIPT=${ROOT_DIR}/scripts/install_packages.R
APP_VERSION=$(cat ${APP_SRC_DIR}/package.json | grep "\"version\"" | cut -f2 -d: | tr "[\",]" " " | tr -d " ")
DIST_ROOT_DIR=${ROOT_DIR}/dist/${APP_VERSION}

echo "Building EcovalApp ${APP_VERSION} for ${OS_TYPE}"

cleanup() {
    cd ${START_DIR}
    rm -rf ${ROOT_BUILD_DIR}
}

abort() {
    echo "ERROR"
    exit 1
}

trap 'cleanup' 0
trap 'abort' ERR

# uncomment to DEBUG:
# set -x

echo "Cleanup source dir.."
bash ${ROOT_DIR}/scripts/clean.sh

echo -n "Prepare build dir.."
mkdir -p ${APP_BUILD_DIR}
for resource in ${APP_RESOURCES}; do
    cp -r ${APP_SRC_DIR}/${resource} ${APP_BUILD_DIR}/
    echo -n "."
done
echo ""

echo -n "Build ${OS_TYPE}.."
# # install/update RPortable dependencies
# cd ${APP_BUILD_DIR}/${R_PORTABLE_REL_DIR}
# ${R_SCRIPT_REL} ${R_INSTALL_PKGS_SCRIPT} > /dev/null
# echo -n "."
cd ${APP_BUILD_DIR}
echo "" # just a placeholder for npm install inline progress infobar
npm install . > /dev/null
echo -n "."
mkdir -p ${DIST_ROOT_DIR}
npm run "package-${OS_TYPE}" > /dev/null  # long: some info goes to stderr
echo -n "."
mv ${DIST_ROOT_DIR}/${DIST_REL_DIR}/LICENSE{,.electron}
mv ${DIST_ROOT_DIR}/${DIST_REL_DIR}/version{,.electron}
cp ${DIST_ROOT_DIR}/${APP_DIST_REL_DIR}/LICENSE ${DIST_ROOT_DIR}/${DIST_REL_DIR}/LICENSE
echo -n "."
npm run "installer-${OS_TYPE}"  # long: some info
# windows-only cleanup, because no opt to leave NuGet (nupkg) file out
rm -rf ${DIST_ROOT_DIR}/EcovalApp-${APP_VERSION}-full.nupkg ${DIST_ROOT_DIR}/RELEASES
echo ""

echo "Cleanup build dir.."
cleanup

echo "DONE"
