EcoValApp
=========

A a user-friendly Graphical User Interface for R CRAN packages `ecoval` and `utility`
which are used for assessment of surface waters. Currently the GUI app supports
assessment of lake shores and of rivers ecomorphology.

Code in this repository contains code and build scripts for Windows and macOS for the
stand-alone EcoValApp, based on the Electron framework.

* LICENSE: GPL-3.0
* Copyright (C) 2017-2022 SIS ID ETH Zurich, SIAM Eawag


Troubleshooting
---------------

If after starting the application you only see a blank window, refresh the application
view by pressing Ctrl+R or Cmd+R, respectively, on Windows or on macOS.
