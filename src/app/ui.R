# ui.R

################################
#### ecoval UI tabs         ####
################################
source("R/ui_tab/home.R", local=TRUE)
source("R/ui_tab/lakes.R", local=TRUE)
source("R/ui_tab/macrophytes.R", local=TRUE)
source("R/ui_tab/morphology.R", local=TRUE)

ecovalUI <- shinyUI(
  fluidPage(theme = "ecoval.css",

    shinyjs::useShinyjs(),  # Set up shinyjs
    navbarPage(uiText$navbar.title,
      homeTabPanel,
      lakesTabPanel,
      macrophytesTabPanel,
      morphologyTabPanel
    ) # navbarPage

  ) # fluidPage
) # shinyUI
