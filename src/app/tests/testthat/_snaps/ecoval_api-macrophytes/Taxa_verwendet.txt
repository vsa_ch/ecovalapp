Erhebung_Code	Stellen_Code	Datum	ArtNr_MSK_WF	Artname_Lateinisch	Abs_Deckung	Rel_Haeufigkeit_Kuenstlich	Unsicherheit_Bestimmung	PhaenZustand_Bluete	PhaenZustand_Fruechte	Bereinigung_Arten	Wuchsform_CH_Kurz	Wuchsform_Bewertung
200_16.08.2016	200	16.08.2016	9300	Agrostis stolonifera L.	1		1				Pgra	Helophyten
200_21.08.2017	200	21.08.2017	9300	Agrostis stolonifera L.	1		1				Pgra	Helophyten
200_16.08.2016	200	16.08.2016	21900	Alisma plantago-aquatica L.	0.25		3	1			Sag	Helophyten
200_16.08.2016	200	16.08.2016	30000001	Callitriche sp.	1.5			1	1	Art(en) ersetzt durch dieses Sammeltaxon	Pep	Aquatische
200_16.08.2016	200	16.08.2016	30000083	Carex 1	0.5					Art(en) ersetzt durch dieses Sammeltaxon	Mcyp	Helophyten
200_16.08.2016	200	16.08.2016	146200	Elodea canadensis Michx.	2		1				Elo	Aquatische
200_21.08.2017	200	21.08.2017	146200	Elodea canadensis Michx.	10		1				Elo	Aquatische
200_16.08.2016	200	16.08.2016	258000	Mentha aquatica L.	1		1	1			Herb	Helophyten
200_21.08.2017	200	21.08.2017	258000	Mentha aquatica L.	0.4		1	1			Herb	Helophyten
200_16.08.2016	200	16.08.2016	30000098	Nasturtium sp. oder Cardamine amara	10			1		Art(en) ersetzt durch dieses Sammeltaxon	Herb	Helophyten
200_21.08.2017	200	21.08.2017	30000098	Nasturtium sp. oder Cardamine amara	8					Art(en) ersetzt durch dieses Sammeltaxon	Herb	Helophyten
200_16.08.2016	200	16.08.2016	297900	Phalaris arundinacea L.	7.75		1	1			Mmono	Helophyten
200_21.08.2017	200	21.08.2017	297900	Phalaris arundinacea L.	1		1				Mmono	Helophyten
200_16.08.2016	200	16.08.2016	338500	Ranunculus fluitans Lam.	35		1	1			Myr	Aquatische
200_21.08.2017	200	21.08.2017	338500	Ranunculus fluitans Lam.	29		1				Myr	Aquatische
200_16.08.2016	200	16.08.2016	378200	Schoenoplectus lacustris (L.) Palla	1		1	1			Mcyp	Helophyten
200_21.08.2017	200	21.08.2017	378200	Schoenoplectus lacustris (L.) Palla	0.2		1	1			Mcyp	Helophyten
200_16.08.2016	200	16.08.2016	379700	Scirpus sylvaticus L.	2		1	1			Mcyp	Helophyten
200_16.08.2016	200	16.08.2016	432100	Typha angustifolia L.	2		3	1	1		Mmono	Helophyten
200_21.08.2017	200	21.08.2017	432100	Typha angustifolia L.	0.5		1	1	1		Mmono	Helophyten
200_16.08.2016	200	16.08.2016	432200	Typha latifolia L.	2		3	1	1		Mmono	Helophyten
200_21.08.2017	200	21.08.2017	432200	Typha latifolia L.	2		1				Mmono	Helophyten
200_21.08.2017	200	21.08.2017	30000120	Veronica anagallis-aquatica oder catenata	1					Art(en) ersetzt durch dieses Sammeltaxon	Herb	Helophyten
200_16.08.2016	200	16.08.2016	441000	Veronica beccabunga L.	2.25		1				Herb	Helophyten
200_21.08.2017	200	21.08.2017	441000	Veronica beccabunga L.	0.2		1				Herb	Helophyten
200_16.08.2016	200	16.08.2016	10000516	Leptodictyum riparium (Hedw.) Warnst.			2				Bry	Moose
200_21.08.2017	200	21.08.2017	10000516	Leptodictyum riparium (Hedw.) Warnst.			2				Bry	Moose
200_16.08.2016	200	16.08.2016	10001401	Fontinalis antipyretica Hedw.			1				Bry	Moose
200_21.08.2017	200	21.08.2017	10001401	Fontinalis antipyretica Hedw.			1				Bry	Moose
200_21.08.2017	200	21.08.2017	30000001	Callitriche sp.	1		1				Pep	Aquatische
200_21.08.2017	200	21.08.2017	30000083	Carex 1	2		1				Mcyp	Helophyten
200_21.08.2017	200	21.08.2017	30000085	Carex 3	2		1				Mcyp	Helophyten
200_16.08.2016	200	16.08.2016	50000001	Bryophyta	1	30	1				BrySum	Moose
200_21.08.2017	200	21.08.2017	50000001	Bryophyta	0.6	80	1				BrySum	Moose
200_16.08.2016	200	16.08.2016	50000004	faedige Gruenalgen	1	30	1				FaedAlg	Algen
200_16.08.2016	200	16.08.2016	60000001	Berula erecta (Huds.) Coville	1		1				Herb	Helophyten
200_21.08.2017	200	21.08.2017	60000001	Berula erecta (Huds.) Coville	0.5		1	1			Herb	Helophyten
200_21.08.2017	200	21.08.2017	30000116	Sparganium 2	0.2					Art(en) ersetzt durch dieses Sammeltaxon	Vall	Aquatische
200_16.08.2016	200	16.08.2016	60000013	Sparganium erectum L. s.l.	2		1	1	1		Mmono	Helophyten
353_23.06.2016	353	23.06.2016	10000535	Hygroamblystegium tenax (Hedw.) Jenn.			2				Bry	Moose
353_23.06.2016	353	23.06.2016	10001401	Fontinalis antipyretica Hedw.			2				Bry	Moose
353_23.06.2016	353	23.06.2016	10002124	Rhynchostegium riparioides (Hedw.) Cardot			2				Bry	Moose
353_23.06.2016	353	23.06.2016	10003415	Fissidens crassipes Bruch & Schimp. subsp. crassipes			2				Bry	Moose
353_23.06.2016	353	23.06.2016	50000001	Bryophyta	9	100	1				BrySum	Moose
353_23.06.2016	353	23.06.2016	50000004	faedige Gruenalgen	1	100	1				FaedAlg	Algen
355_26.05.2016	355	26.05.2016	10000516	Leptodictyum riparium (Hedw.) Warnst.			2				Bry	Moose
355_26.05.2016	355	26.05.2016	10002124	Rhynchostegium riparioides (Hedw.) Cardot			2				Bry	Moose
355_26.05.2016	355	26.05.2016	10003297	Cratoneuron filicinum (Hedw.) Spruce			2				Bry	Moose
355_26.05.2016	355	26.05.2016	50000001	Bryophyta	2	50	1				BrySum	Moose
463_04.07.2016	463	04.07.2016	9300	Agrostis stolonifera L.	1		1				Pgra	Helophyten
463_04.07.2016	463	04.07.2016	191700	Glyceria notata Chevall.	1		1	1			Pgra	Helophyten
463_04.07.2016	463	04.07.2016	30000098	Nasturtium sp. oder Cardamine amara	1			1		Art(en) ersetzt durch dieses Sammeltaxon	Herb	Helophyten
463_04.07.2016	463	04.07.2016	297900	Phalaris arundinacea L.	5		1	1			Mmono	Helophyten
463_04.07.2016	463	04.07.2016	441000	Veronica beccabunga L.	2		1				Herb	Helophyten
463_04.07.2016	463	04.07.2016	10001401	Fontinalis antipyretica Hedw.			2				Bry	Moose
463_04.07.2016	463	04.07.2016	10002124	Rhynchostegium riparioides (Hedw.) Cardot			2				Bry	Moose
463_04.07.2016	463	04.07.2016	10003415	Fissidens crassipes Bruch & Schimp. subsp. crassipes			2				Bry	Moose
463_04.07.2016	463	04.07.2016	50000001	Bryophyta	1	70	1				BrySum	Moose
5640_18.07.2017	5640	18.07.2017	267800	Myriophyllum spicatum L.	2		1				Myr	Aquatische
5640_18.07.2017	5640	18.07.2017	338500	Ranunculus fluitans Lam.	7.5		1				Myr	Aquatische
5640_18.07.2017	5640	18.07.2017	10000516	Leptodictyum riparium (Hedw.) Warnst.			2				Bry	Moose
5640_18.07.2017	5640	18.07.2017	10001401	Fontinalis antipyretica Hedw.			2				Bry	Moose
5640_18.07.2017	5640	18.07.2017	10002124	Rhynchostegium riparioides (Hedw.) Cardot			2				Bry	Moose
5640_18.07.2017	5640	18.07.2017	10003415	Fissidens crassipes Bruch & Schimp. subsp. crassipes			2				Bry	Moose
5640_18.07.2017	5640	18.07.2017	50000001	Bryophyta	1	100	1				BrySum	Moose
5640_18.07.2017	5640	18.07.2017	50000004	faedige Gruenalgen	0.5	0	1				FaedAlg	Algen
5724_27.06.2016	5724	27.06.2016	9300	Agrostis stolonifera L.	1		1				Pgra	Helophyten
5724_27.06.2016	5724	27.06.2016	30000083	Carex 1	10			1		Art(en) ersetzt durch dieses Sammeltaxon	Mcyp	Helophyten
5724_27.06.2016	5724	27.06.2016	191700	Glyceria notata Chevall.	2		1	1			Pgra	Helophyten
5724_27.06.2016	5724	27.06.2016	267900	Myriophyllum verticillatum L.	2		2				Myr	Aquatische
5724_27.06.2016	5724	27.06.2016	30000098	Nasturtium sp. oder Cardamine amara	3			1		Art(en) ersetzt durch dieses Sammeltaxon	Herb	Helophyten
5724_27.06.2016	5724	27.06.2016	297900	Phalaris arundinacea L.	10		1	1			Mmono	Helophyten
5724_27.06.2016	5724	27.06.2016	30000120	Veronica anagallis-aquatica oder catenata	5			1		Art(en) ersetzt durch dieses Sammeltaxon	Herb	Helophyten
5724_27.06.2016	5724	27.06.2016	50000004	faedige Gruenalgen	2	0	1				FaedAlg	Algen
5724_27.06.2016	5724	27.06.2016	60000013	Sparganium erectum L. s.l.	5		1	1			Mmono	Helophyten
5893_27.06.2016	5893	27.06.2016	30000098	Nasturtium sp. oder Cardamine amara	0.2					Art(en) ersetzt durch dieses Sammeltaxon	Herb	Helophyten
5893_27.06.2016	5893	27.06.2016	297900	Phalaris arundinacea L.	0.2		1	1			Mmono	Helophyten
5893_27.06.2016	5893	27.06.2016	10000516	Leptodictyum riparium (Hedw.) Warnst.			2				Bry	Moose
5893_27.06.2016	5893	27.06.2016	10001401	Fontinalis antipyretica Hedw.			2				Bry	Moose
5893_27.06.2016	5893	27.06.2016	10002124	Rhynchostegium riparioides (Hedw.) Cardot			2				Bry	Moose
5893_27.06.2016	5893	27.06.2016	50000001	Bryophyta	4	0	1				BrySum	Moose
5893_27.06.2016	5893	27.06.2016	50000004	faedige Gruenalgen	1	0	1				FaedAlg	Algen
9222_04.08.2017	9222	04.08.2017	60000013	Sparganium erectum L. s.l.	60	120	1	1			Mmono	Helophyten
9222_04.08.2017	9222	04.08.2017	297900	Phalaris arundinacea L.	5		1				Mmono	Helophyten
9222_04.08.2017	9222	04.08.2017	441000	Veronica beccabunga L.	1		1				Herb	Helophyten
9222_04.08.2017	9222	04.08.2017	319200	Potamogeton natans L.	4		1				Mnym	Aquatische
9222_04.08.2017	9222	04.08.2017	30000116	Sparganium 2	5					Art(en) ersetzt durch dieses Sammeltaxon	Vall	Aquatische
9186_17.08.2017	9186	17.08.2017	50000004	faedige Gruenalgen	10	20	1				FaedAlg	Algen
9186_17.08.2017	9186	17.08.2017	50000001	Bryophyta	0.1	100	1				BrySum	Moose
9186_17.08.2017	9186	17.08.2017	10001401	Fontinalis antipyretica Hedw.	0.1	100	1				Bry	Moose
9186_17.08.2017	9186	17.08.2017	319300	Potamogeton nodosus Poir.	5		1				Mnym	Aquatische
9186_17.08.2017	9186	17.08.2017	319700	Potamogeton pectinatus L.	5		1				Ppot	Aquatische
9186_17.08.2017	9186	17.08.2017	30000115	Sparganium 1	0.2					Art(en) ersetzt durch dieses Sammeltaxon	Mmono	Helophyten
9186_17.08.2017	9186	17.08.2017	30000001	Callitriche sp.	0.1		1				Pep	Aquatische
9186_17.08.2017	9186	17.08.2017	378200	Schoenoplectus lacustris (L.) Palla	0.1		1				Mcyp	Helophyten
9186_17.08.2017	9186	17.08.2017	30000098	Nasturtium sp. oder Cardamine amara	0.1			1		Art(en) ersetzt durch dieses Sammeltaxon	Herb	Helophyten
