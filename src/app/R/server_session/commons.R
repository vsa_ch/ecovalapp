# server_session/commons.R
#
# Common utilities sourced in server.R session
#


getFileURL.copyingIssueHandler <- function(cond) {
  message("Cannot copy output file to the staging folder: ",
    conditionMessage(cond))
  FALSE
}
getFileURL.createLinkingIssueHandler <- function(from, to) function(cond) {
  message("Cannot link output file in the staging folder: ",
    conditionMessage(cond))
  message("Copying output file instead.")
  handled <- tryCatch(
    {
      # no need to overwrite, because `unlink` should have been called first
      file.copy(from, to)
      TRUE
    },
    error=getFileURL.copyingIssueHandler,
    warning=getFileURL.copyingIssueHandler)
  print(paste("Linking issue handled?", handled))
  handled
}
getFileURL <- function(filePath) {
  # Problem: linking to local server-side resources is disabled in all modern
  #          browsers due to security restrictions.
  # Workaround: (hard) link to the file in the safe "www/" folder (using
  #             file's basename) and return path of the hard link
  # TODO: consider a Windows-specific workaround - see: https://stackoverflow.com/a/14249649/207241
  linkPath <- basename(filePath)
  stagingPath <- file.path(stagingDir, linkPath)
  unlink(stagingPath)
  linkingIssueHandler <- getFileURL.createLinkingIssueHandler(filePath,
    stagingPath)
  isStaged <- tryCatch(
    {
      # fails e.g. when linking files between different Windows drives
      file.link(filePath, stagingPath)
      TRUE
    },
    error=linkingIssueHandler,
    warning=linkingIssueHandler)
  return(if (isStaged) linkPath else NA)
}
getFilesListURL <- function(dirPath, filePattern="^.+\\.[^\\.]+") {

  # link all files with a given pattern
  links <- sapply(list.files(dirPath, filePattern, full.names = TRUE),
    getFileURL, USE.NAMES = FALSE)

  # prep HTML with index of all staged files (OPT: use modal popups instead)
  ulTag <- do.call(tags$ul,
    lapply(links, function(link) tags$li(tagPopoupLink(link))))
  docTag <- tags$html(tags$body(tags$h1(uiText$documentation), ulTag))

  # write HTML to file
  linkPath <- "doc.html"
  fileConn <- file(file.path(stagingDir, linkPath))
  # Note: overwrites previous content of the file
  writeLines(as.character(docTag), fileConn)
  close(fileConn)

  return(linkPath)
}
prepLogFile <- function(logBaseName, resWrap) {
  # Note: using only `tempdir()` and not `tempfile()` to minimize amount of
  #       created files, because we don't remove them (yet?)
  logFile <- file.path(tempdir(), paste0(logBaseName,"Log.txt"))
  # Delete should not be necessary, as write overwrites previous contents
  # if (file.exists(logFile)) file.remove(logFile)
  write(outputLogger$getLogStr(resWrap), file=logFile)
  return(logFile)
}


tagBrokenLink <- function(link.txt=uiText$broken.link) {
  tags$span(style="text-decoration: line-through;", link.txt)
}
tagPopoupLink <- function(url, link.txt=NA) {
  if (is.na(link.txt)) link.txt = url
  if (is.na(url))
    tagBrokenLink(link.txt)
  else
    tags$a(target="_blank", href=url, link.txt)
}
tagFileLink <- function(link.txt, file.name, dir.name) {
  url = getFileURL(file.path(dir.name, file.name))
  tagPopoupLink(url, link.txt)
}
tagFilesListLink <- function(link.txt, file.name.pattern, dir.name) {
  url = getFilesListURL(dir.name, file.name.pattern)
  tagPopoupLink(url, link.txt)
}


tagFootnote <- function(content) tags$div(class="small", content)


tagAlertBox <- function(type="info", ...)
  tags$div(class=paste0("alert alert-",type), ...)
tagInfoBox <- function(...) tagAlertBox(type="info", ...)
tagWarnBox <- function(...) tagAlertBox(type="warning", ...)
tagDoneBox <- function(...) tagAlertBox(type="success", ...)
tagErrorBox <- function(...) tagAlertBox(type="danger", ...)


tagMsg <- function(tagBox, msg, logFileURL=NA, more.txt=uiText$r.console.output)
  tagBox(
    tags$p(msg, if (is.na(logFileURL)) NULL
      else tagFootnote(tags$a(target = "_blank", href = logFileURL, more.txt)))
  )
tagInfoMsg <- function(info.msg, ...)
  tagMsg(tagInfoBox, info.msg, ...)
tagPlaceholderMsg <- function(placeholder.msg=HTML("&nbsp;"), more.txt=HTML("&nbsp;"))
  tagInfoMsg(info.msg=placeholder.msg, logFileURL="", more.txt=more.txt)
tagWarnMsg <- function(warn.msg, ...)
  tagMsg(tagWarnBox, warn.msg, ...)
tagDoneMsg <- function(done.msg=uiText$success, ...)
  tagMsg(tagDoneBox, done.msg, ...)
tagErrorMsg <- function(err.msg=uiText$error, ...)
  tagMsg(tagErrorBox, err.msg, ...)


isInputValueMissing <- function(value) {
  is.null(value) || helpers$valueNotGiven(value)
}
hasInputValueChanged <- function(oldValue, newValue) {
  if (!isInputValueMissing(oldValue))
    isInputValueMissing(newValue) || oldValue != newValue
  else # isInputValueMissing(oldValue)
    !isInputValueMissing(newValue)
}
validateInputGiven <- function(inputName, alertOutputName, alertMsg,
  getInputValueFunc=getInputValue) {
  # check if sites data is given
  if (isInputValueMissing(getInputValueFunc(inputName))) {
    output[[alertOutputName]] <- renderUI(tagWarnMsg( alertMsg ))
    return(FALSE)
  }
  return(TRUE)
}


# FIXME: re-design to use reactive elements in separate reactive functions, not
#        explicit updates calls
#   * temp. disabled validation of river types and attributes because
#     `update*Input` functions seemed no to affect input[[inputName]] value in
#     the same top-level reactive function call chain
#   * caching of inputs w/ getters is another, more general workaround (cf. 7dff443)
#   * cf. https://stackoverflow.com/a/40622303/207241
getInputValue <- function(inputName) {
  return(input[[inputName]])
}
updateNumericInputWrapper <- function(inputName, value) {
  dispValue = input[[inputName]]
  updateNumericInput(session, inputName, value=value)
  return(list(previous=dispValue, current=value))
}
isInputValueValidChoice <- function(value, choices) {
  !isInputValueMissing(value) && (value %in% choices)
}
updateSelectInputWrapper <- function(inputName, choices=NULL, selected=NULL,
  auto.select.first = TRUE, ...)
{
  dispValue = getInputValue(inputName)
  if (is.null(choices)) {
    choices = character(0)
    # ignore also selected value if no choices were given
    selected = ""
  } else if (!((is.character(selected) && (selected == "")) || isInputValueValidChoice(selected, choices))) {
    # if not a valid selection, auto-select first choice by default
    selected = if (isInputValueValidChoice(dispValue, choices)) dispValue
      else if (auto.select.first) head(choices, 1) else ""
  }
  updateSelectizeInput(session, inputName, choices=choices, selected=selected,
    ...)
  # return previous and selected input values
  return(list(previous=dispValue, current=selected))
}


# write output file wrapper
writeWrapper <- function(apiFun, outDir, fileName, outFileExt, ..., logFilePrefix="")
{
  outFileName <- paste0(fileName, outFileExt)

  resWrap <- apiFun(outDir, outFileName, ...)

  fn <- if (outputLogger$hasError(resWrap)) {
    # Note: not writing any logs to outDir, because we never checked that we can
    prepLogFile(paste0(logFilePrefix, fileName), resWrap)
  } else {
    outFileName
  }

  return(
    # Use data.frame with bool success flag for an easy row-binding and filtering of all
    # write actions
    data.frame(
      success = (fn == outFileName),
      fn = fn
    )
  )
}
writeTagMsg <- function(res.write.df, logFileURL) {
  if (!all(res.write.df$success)) {
    ind.write.err <- min(which(!res.write.df$success))
    log.fn <- res.write.df$fn[ind.write.err]
    tagErrorMsg(err.msg=uiText$write.error, logFileURL=getFileURL(log.fn))
  } else {
    tagDoneMsg(done.msg=uiText$read.success, logFileURL=logFileURL)
  }
}


# use reactiveVal(NA) to avoid <<-?
lastDirectoryPath <- NA
getDirectoryInputValue <- function(inputName) {
  path <- readDirectoryInput(session, inputName)
  path
}
chooseDirWrapper <- function(inputName) {
  path <- getDirectoryInputValue(inputName)
  if (isInputValueMissing(path))
    path <- lastDirectoryPath
  choose.dir(default=path, caption=uiText$select.directory)
}
updateDirectoryInputWrapper <- function(inputName) {
  updated <- FALSE
  path <- NA
  # condition prevents handler execution on initial app launch
  if (getInputValue(inputName) > 0) {
    path <- chooseDirWrapper(inputName)
    updated <- !is.na(path)
    if (updated)
      updateDirectoryInput(session, inputName, value=path)
  }
  lastDirectoryPath <<- path
  return(list(updated=updated, path=path))
}
updateInputFileNames <- function(
    inputDirName,
    inputs,
    choices,
    dirPath=NA
) {
  dirInput = getInputValue(inputDirName)
  if (dirInput > 0) {
    if (is.na(dirPath))
      dirPath = getDirectoryInputValue(inputDirName)
    file.names = list.files(dirPath, "\\.[^\\.]+$", full.names = FALSE)
    # update only if choices really changed
    if (length(choices) != length(file.names) ||
        any(choices != file.names))
    {
      choices <- file.names
      for (input in inputs) {
        updateSelectInputWrapper(input, file.names, auto.select.first=FALSE,
          server=TRUE)
        # load choices form server (server=TRUE), otherwise the dropdown will
        # close immediately after updateSelectizeInput call and
        # onevent("focusin",...) update (below) won't be usable
      }
    }
  }
  return(choices)
}


# temporarily swap running box and action output info boxes on each click
actionRunningOff <- function(actionPrefix) {
  shinyjs::hide(id=paste0(actionPrefix,"Running"))
  shinyjs::show(id=paste0(actionPrefix,"Output"))
}
actionRunningOn <- function(actionPrefix) {
  shinyjs::show(id=paste0(actionPrefix,"Running"))
  shinyjs::hide(id=paste0(actionPrefix,"Output"))
}


updateOutLink <- function(done.before, resOutputName, link.txt, linkFun, fn,
  ...)
isolate({
  if (!isInputValueMissing(fn))
    output[[resOutputName]] <- renderUI(tags$p(
      tags$strong(linkFun(link.txt, fn, ...))
    ))
  else if (!done.before)
    output[[resOutputName]] <- renderUI(tags$p(link.txt))
})
updateOutFileLink <- function(done.before, resOutputName, out.dir, fn,
  link.txt)
  updateOutLink(done.before, resOutputName, link.txt, tagFileLink, fn,
    out.dir)
updateOutFilesListLink <- function(done.before, resOutputName, out.dir,
  fn.pattern, link.txt)
  updateOutLink(done.before, resOutputName, link.txt, tagFilesListLink,
    fn.pattern, out.dir)


actionOutputsReset <- function(
  doneOutput,
  alertOutputNamesToReset = names(doneOutput),
  alertOutputNamesToTag = c(),
  tag = NULL,
  tag.otherwise = tagPlaceholderMsg()
) {
  sapply(alertOutputNamesToReset, function(alertOutputName) {
    doneOutput[[alertOutputName]] <- FALSE
    alertOutputTag = if (!is.null(tag) && (alertOutputName %in% alertOutputNamesToTag))
      tag else tag.otherwise
    output[[alertOutputName]] <- renderUI(alertOutputTag)
  })
  NULL
}


plotMsg <- function(msg) {
    # print(paste0("plotMsg: \"", msg, "\""))
    plot.new()
    text(.5,.5, msg)
}
