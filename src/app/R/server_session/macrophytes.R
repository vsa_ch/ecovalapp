# server_session/macrophytes.R
#
# Macrophytes module handlers sourced in server.R session
#

## State shared between actions
##
# probably one should use here reactiveValues()
macrophytes.doneOutput <- list(
  "macrophytesCheckFilesOutput" = FALSE,
  "macrophytesComputeTypificationOutput" = FALSE,
  "macrophytesEvaluatePrelimOutput" = FALSE,
  "macrophytesEvaluatePlausOutput" = FALSE
)
macrophytes.checkAndEvalRes <- NULL
macrophytesUpdateResults <- function(res) {
  macrophytes.checkAndEvalRes <<- res
}

## Helpers
##
# input files and directories
macrophytes.fileNameInputs <- c(
  "macrophytesSitesFileName",
  "macrophytesSpeciesFileName",
  "macrophytesPlausibilityFileName")
macrophytes.fileNameChoices <- c()
macrophytesUpdateInputFileNames <- function(dirPath=NA, inputs=macrophytes.fileNameInputs) {
  macrophytes.fileNameChoices <<- updateInputFileNames(
      "macrophytesInputDataDir", inputs, macrophytes.fileNameChoices, dirPath=dirPath)

  dirInput = getInputValue("macrophytesInputDataDir")
  if (dirInput > 0) {
    if (is.na(dirPath))
      dirPath = getDirectoryInputValue('macrophytesInputDataDir')
    file.names = list.files(dirPath, "\\.[^\\.]+$", full.names = FALSE)
    # update only if choices really changed
    if (length(macrophytes.fileNameChoices) != length(file.names) ||
        any(macrophytes.fileNameChoices != file.names))
    {
      macrophytes.fileNameChoices <<- file.names
      for (input in inputs) {
        updateSelectInputWrapper(input, file.names, auto.select.first=FALSE,
          server=TRUE)
        # load choices form server (server=TRUE), otherwise the dropdown will
        # close immediately after updateSelectizeInput call and
        # onevent("focusin",...) update (below) won't be usable
      }
    }
  }
}
# be careful: `for (fn in macrophytes.fileNameInputs)` won't work here
shinyjs::onevent("focusin", "macrophytesSitesFileName",
    macrophytesUpdateInputFileNames())
shinyjs::onevent("focusin", "macrophytesSpeciesFileName",
    macrophytesUpdateInputFileNames())
shinyjs::onevent("focusin", "macrophytesPlausibilityFileName",
    macrophytesUpdateInputFileNames())

observeEvent(input$macrophytesInputDataDir, {
  dir = updateDirectoryInputWrapper('macrophytesInputDataDir')
  if (dir$updated) {
    # on dir change: reset selections if files do not exist in the new dir
    # pass directly new path as the input's value might not be up to date yet
    macrophytesUpdateInputFileNames(dirPath=dir$path)
  }
})
observeEvent(input$macrophytesInputImgDir, {
  updateDirectoryInputWrapper('macrophytesInputImgDir')
})
observeEvent(input$macrophytesOutputDir, {
  updateDirectoryInputWrapper('macrophytesOutputDir')
})

# results links
macrophytesUpdateOutLinks <- function(out.dir="", fn.numeric=NA, fn.used.taxa=NA, fn.discarded.taxa=NA, fn.quality.checks=NA,
  fn.documentation=NA)
isolate({
  read.done = !is.null(macrophytes.checkAndEvalRes)
  doc.done = read.done && getInputValue("macrophytesIfWriteDoc")
  eval.done = macrophytesAPI$hasTypification(macrophytes.checkAndEvalRes)
  updateOutFileLink(eval.done, "macrophytesNumResOutput", out.dir,
    fn.numeric, uiText$numeric)
  updateOutFileLink(eval.done, "macrophytesUsedTaxaOutput", out.dir,
    fn.used.taxa, uiText$used.taxa)
  updateOutFileLink(eval.done, "macrophytesDiscardedTaxaOutput", out.dir,
    fn.discarded.taxa, uiText$discarded.taxa)
  updateOutFileLink(read.done, "macrophytesQualityChecksOutput", out.dir,
    fn.quality.checks, uiText$quality.checks)
  fn.doc.pattern = if (is.na(fn.documentation)) NA else paste0("^.+",fn.documentation)
  updateOutFilesListLink(doc.done, "macrophytesDocumentationOutput",
    out.dir, fn.doc.pattern, uiText$documentation
  )
})
# initial text placeholders
macrophytesUpdateOutLinks()

# validate inputs pre check action
macrophytesValidatePreCheck <- function(alertOutputName) {
  res = list()

  # print("DEBUG")
  # print("macrophytesValidatePreCheck")
  # print(paste("  input dir =", getDirectoryInputValue("macrophytesInputDataDir")))
  # print(paste("  sites data =", getInputValue("macrophytesSitesFileName")))
  # print(paste("  output dir =", getDirectoryInputValue("macrophytesOutputDir")))

  # check if input dirs are chosen
  if (
    !validateInputGiven("macrophytesInputDataDir", alertOutputName,
      uiText$input.dir.first, getInputValueFunc=getDirectoryInputValue)
  )
    return(NULL)
  # if (getInputValue("macrophytesIfWriteDoc") &&
  #   !validateInputGiven("macrophytesInputImgDir", alertOutputName,
  #     uiText$input.dir.first, getInputValueFunc=getDirectoryInputValue))
  #   return(NULL)
  # TODO: reset macrophytes.doneOutput on change of one of the input
  #       dirs or input files names, language, sampling protocol etc. ?
  res$inputDataDirPath <- getDirectoryInputValue("macrophytesInputDataDir")
  res$inputImgDirPath <- getDirectoryInputValue("macrophytesInputImgDir")

  # check if sites data is given
  if (
    !validateInputGiven("macrophytesSitesFileName", alertOutputName,
      paste(uiText$missing.filename, uiText$sites.data, sep=": "))
  )
    return(NULL)

  # check if the output dir is chosen
  if (
    !validateInputGiven("macrophytesOutputDir", alertOutputName,
      uiText$output.dir.first, getInputValueFunc=getDirectoryInputValue)
  )
    return(NULL)
  res$outputDirPath <- getDirectoryInputValue("macrophytesOutputDir")

  return(res)
}

# validate inputs pre evaluation actions
macrophytesValidatePreEvaluate <- function(alertOutputName, sampling=TRUE, ...) {
  # OPT, require separate, quicker data check prior to actual evaluation
  # if (sampling)
  #   # check first if the input data check was successfully done
  #   if (!macrophytes.doneOutput[["macrophytesCheckFilesOutput"]]) {
  #     output[[alertOutputName]] <- renderUI(tagWarnBox(uiText$check.first))
  #     return(NULL)
  #   }

  return(macrophytesValidatePreCheck(alertOutputName))
}

macrophytesPrepareEvaluationArgs <- function(dirs, sampling=TRUE,
  species=NULL, plaus=NULL) {
  if (is.null(species))
    species = !isInputValueMissing(getInputValue("macrophytesSpeciesFileName"))
  if (is.null(plaus))
    plaus = !isInputValueMissing(getInputValue("macrophytesPlausibilityFileName"))
  list(
    getInputValue("macrophytesLanguage"),
    dirs$inputDataDirPath,
    dirs$inputImgDirPath,
    getInputValue("macrophytesInputSeparator"),
    helpers$appendFnExtIfMissing( # inFileSites
      getInputValue("macrophytesSitesFileName"),
      macrophytesAPI$defaultFileExtensions$input.file.site),
    samplingProtocol = getInputValue("macrophytesSamplingProtocol"),
    outputDir = dirs$outputDirPath,
    outputSeparator = getInputValue("macrophytesOutputSeparator"),
    outFileQualityChecks = helpers$appendFnExtIfMissing(
      getInputValue("macrophytesQualityChecksFileName"),
      macrophytesAPI$defaultFileExtensions$output.file.quality.checks),
    outFileDoc = if (getInputValue("macrophytesIfWriteDoc")) helpers$appendFnExtIfMissing(
      getInputValue("macrophytesDocFileName"),
      macrophytesAPI$defaultFileExtensions$output.file.doc) else NA,
    # sampling-dependent
    sampleSize = if (sampling) as.numeric(getInputValue("macrophytesNSamples")) else 0,
    outFileNumeric = if (sampling) helpers$appendFnExtIfMissing(
      getInputValue("macrophytesNumResFileName"),
      macrophytesAPI$defaultFileExtensions$output.file.numeric) else NA,
    # species-dependent
    inFileSpecies = if (species) helpers$appendFnExtIfMissing(
      getInputValue("macrophytesSpeciesFileName"),
      macrophytesAPI$defaultFileExtensions$input.file.species) else NA,
    outFileTaxaUsed = if (species) helpers$appendFnExtIfMissing(
      getInputValue("macrophytesUsedTaxaFileName"),
      macrophytesAPI$defaultFileExtensions$output.file.taxa.used) else NA,
    outFileTaxaRemoved = if (species) helpers$appendFnExtIfMissing(
      getInputValue("macrophytesDiscardedTaxaFileName"),
      macrophytesAPI$defaultFileExtensions$output.file.taxa.removed) else NA,
    # plaus-dependent
    inFilePlausibleType = if (plaus) helpers$appendFnExtIfMissing(
      getInputValue("macrophytesPlausibilityFileName"),
      macrophytesAPI$defaultFileExtensions$input.file.typeplaus) else NA
  )
}

macrophytesEvaluateWrapper <- function(alertOutputName, ...) {
  # validate state pre-evaluate
  valid <- macrophytesValidatePreEvaluate(alertOutputName, ...)
  if (is.null(valid)) return(FALSE)

  # evaluate
  args = macrophytesPrepareEvaluationArgs(valid, ...)
  #n.in = length(macrophytesAPI$getObservationCodes(macrophytes.checkAndEvalRes))
  #n.logsamp = max(0,log10(args$sampleSize)-1)
  #t.wait=ceiling(5+0.3*n.in*n.logsamp) # w/ n.in=50, for n.samp = 0: 5s, 100: 20s, 1000: 35s, 10000: 50s
  progress <- Progress$new(session, min=1, max=3)
  on.exit(progress$close())
  progress$set(message = paste(uiText$evaluating, uiText$please.wait, sep=".. "),
                detail = '')
  progress$set(value = 1)
  Sys.sleep(0.1)
  progress$set(value = 2)
  resWrap <- do.call(macrophytesAPI$readInputFilesEvaluateAndWrite, args)
  logFileURL <- getFileURL(prepLogFile(alertOutputName, resWrap))
  progress$set(value = 3)

  # check for errors
  if (outputLogger$hasError(resWrap)) {
    output[[alertOutputName]] <- renderUI(tagErrorMsg(logFileURL=logFileURL))
    return(FALSE)
  }

  res <- outputLogger$getValue(resWrap)

  # explicitly reset intereactive changes buffer
  macrophytesAPI$modBuffer$clear()

  # update UI
  macrophytesUpdateOutLinks(valid$outputDirPath, fn.numeric = args$outFileNumeric,
    fn.used.taxa = args$outFileTaxaUsed,
    fn.discarded.taxa = args$outFileTaxaRemoved,
    fn.quality.checks = args$outFileQualityChecks,
    fn.documentation = args$outFileDoc)
  macrophytesUpdateObservationCodeSelector(res)
  # Since there are observeEvents and chained updates there is no need to call
  # here either of:
  #     macrophytesUpdateModificationAttributes(res)
  #     macrophytesUpdatePlotsFromInputs(...)

  # update global state (after UI update, otherwise links won't get updated)
  macrophytesUpdateResults(res)
  macrophytes.doneOutput[[alertOutputName]] <<- TRUE

  # write success or non-fatal error msg, and link the log
  nonFatalError = outputLogger$hasLogError(resWrap)
  output[[alertOutputName]] <- if (nonFatalError) {
    quality.checks.link = tagFileLink(
      uiText$quality.checks, # bit too long: paste(uiText$see, uiText$quality.checks)
      args$outFileQualityChecks,
      valid$outputDirPath)
    renderUI(tagErrorMsg(
      err.msg=tags$span(uiText$error, tags$strong(quality.checks.link)),
      logFileURL=logFileURL))
  } else renderUI(tagDoneMsg(logFileURL=logFileURL))

  return(TRUE)
}

# reset results incl. actions status and output links, but not the modifications buffer
macrophytesActionOutputsReset <- function(...)
{
  macrophytesUpdateResults(NULL)
  macrophytesUpdateOutLinks()  # unconditionally reset all outputs
  actionOutputsReset(macrophytes.doneOutput, ...)
}
# initial reset
macrophytesActionOutputsReset()


#### BEGIN: evaluations
# tech: `onclick` event update comes only after UI update from observeEvent;
#       using `mousedown` event instead
shinyjs::onevent('mousedown', "macrophytesCheckFilesAction",
  actionRunningOn("macrophytesCheckFiles"))
shinyjs::onevent('mousedown', "macrophytesComputeTypificationAction",
  actionRunningOn("macrophytesComputeTypification"))
shinyjs::onevent('mousedown', "macrophytesEvaluatePrelimAction",
  actionRunningOn("macrophytesEvaluatePrelim"))
shinyjs::onevent('mousedown',"macrophytesEvaluatePlausAction",
  actionRunningOn("macrophytesEvaluatePlaus"))
observeEvent(input$macrophytesCheckFilesAction, isolate({
  # reset all previous outputs
  macrophytesActionOutputsReset()
  alertOutputName = "macrophytesCheckFilesOutput"
  # eval w/o sampling
  eval_ok = macrophytesEvaluateWrapper(alertOutputName, sampling=FALSE)
  if (eval_ok) macrophytesUpdatePlotsFromInputs()
  actionRunningOff("macrophytesCheckFiles")
}))
observeEvent(input$macrophytesComputeTypificationAction, isolate({
  # reset all previous outputs
  macrophytesActionOutputsReset()
  alertOutputName = "macrophytesComputeTypificationOutput"
  eval_ok = macrophytesEvaluateWrapper(alertOutputName, species=FALSE, plaus=FALSE)
  if (eval_ok) macrophytesUpdatePlotsFromInputs()
  actionRunningOff("macrophytesComputeTypification")
}))
observeEvent(input$macrophytesEvaluatePrelimAction, isolate({
  # reset all previous outputs
  macrophytesActionOutputsReset()
  alertOutputName = "macrophytesEvaluatePrelimOutput"
  # check if species data is given
  if (validateInputGiven("macrophytesSpeciesFileName", alertOutputName,
    paste(uiText$missing.filename, uiText$species.data, sep=": "))) {
    eval_ok = macrophytesEvaluateWrapper(alertOutputName, species=TRUE, plaus=FALSE)
    if (eval_ok) macrophytesUpdatePlotsFromInputs()
  }
  actionRunningOff("macrophytesEvaluatePrelim")
}))
observeEvent(input$macrophytesEvaluatePlausAction, isolate({
  # reset all previous outputs
  macrophytesActionOutputsReset()
  alertOutputName = "macrophytesEvaluatePlausOutput"
  # check if species and plausibility data are given
  if (
    validateInputGiven("macrophytesSpeciesFileName", alertOutputName,
      paste(uiText$missing.filename, uiText$species.data, sep=": ")) &&
    validateInputGiven("macrophytesPlausibilityFileName", alertOutputName,
      paste(uiText$missing.filename, uiText$plausibilised.river.types, sep=": "))
  ) {
    eval_ok = macrophytesEvaluateWrapper(alertOutputName, species=TRUE, plaus=TRUE)
    if (eval_ok) macrophytesUpdatePlotsFromInputs()
  }
  actionRunningOff("macrophytesEvaluatePlaus")
}))
### END: evaluations


#### BEGIN: interactive plots
## interactive modifications: update inputs
##
macrophytesUpdateObservationCodeSelector <- function(res) isolate({
  iv = updateSelectInputWrapper("macrophytesObservationCode",
    macrophytesAPI$getObservationCodes(res))
  # Since there is observeEvent on the observation code selector, call
  # explicitly attribute update only when update was triggered but observation
  # code hasn't changed (e.g. when re-computed typification)
  if (!hasInputValueChanged(iv$previous, iv$current))
    macrophytesUpdateModificationAttributes(res)
  NULL
})

#' Update all modfiable attribiutes inputs based on the API buffer and results
#' object
macrophytesUpdateModificationAttributes <- function(res) isolate({
  macrophytesUpdateTypificationAttribute(res)
  macrophytesUpdateAssesmentAttribute(res)
})
#' Update typfication attribiutes inputs (name and value) based on the API
#' buffer and results object
macrophytesUpdateTypificationAttribute <- function(res) isolate({
  iv = updateSelectInputWrapper("macrophytesTypificationAttribute",
    macrophytesAPI$getAttributeTypes(res))
  # Since there is observeEvent on the attribute selector, call explicitly
  # attribute value update only when update was triggered but attribute
  # hasn't changed (e.g. when chosen a different observation)
  # or
  # when the SelectInput hasn't yest change input (because of isolate?)
  if (!hasInputValueChanged(iv$previous, iv$current) ||
    hasInputValueChanged(getInputValue("macrophytesTypificationAttribute"), iv$current))
  {
    macrophytesUpdateTypificationAttributeValue(res, typification.attribute=iv$current)
  }
  NULL
})
#' Update typfication attribiute value input based on the API buffer and
#' results object
macrophytesUpdateTypificationAttributeValue <- function(res,
  typification.attribute=NULL, update.plot=NULL)
isolate({
  if (is.null(typification.attribute))
    typification.attribute = getInputValue("macrophytesTypificationAttribute")
  inputName = "macrophytesTypificationAttributeValue"
  inputValue = getInputValue(inputName)
  if (!is.null(res) && !isInputValueMissing(getInputValue("macrophytesObservationCode")) &&
      !isInputValueMissing(typification.attribute))
  {
    newValue = macrophytesAPI$getObservationAttributeTypeValue(res,
      getInputValue("macrophytesObservationCode"),
      typification.attribute)
    # OPT: change `min`/`max` and `step` kwargs here dep. on the
    #      `typification.attribute` value
    iv = updateNumericInputWrapper(inputName, newValue)
    # print(iv)
    # There is observeEvent on the typification attribute value
    if.update.plot = ifUpdatePlotDespiteObserveEvent(iv$previous, iv$current,
      update.now=update.plot)
    iv[["update.plot.pending"]] = if.update.plot$pending
    if (if.update.plot$now)
      macrophytesUpdateTypificationPlots(getInputValue("macrophytesTypificationPlotChoice"))
  } else {
    iv = list(previous=inputValue, current=inputValue, update.plot.pending=FALSE)
  }
  iv
})
#' Update assesment attribiutes inputs (name and value) based on the API
#' buffer and results object
macrophytesUpdateAssesmentAttribute <- function(res) isolate({
  # print("macrophytesUpdateAssesmentAttribute")
  iv = updateSelectInputWrapper("macrophytesAssesmentAttribute",
    macrophytesAPI$getAttributeSpecies(res))
  # print(iv)
  # Since there is observeEvent on the attribute selector, call explicitly
  # attribute value update only when update was triggered but attribute
  # hasn't changed (e.g. when chosen a different observation)
  # or
  # when the SelectInput hasn't yet changed input (because of isolate?)
  iv.attr = list(update.plot.pending = NULL)
  if (!hasInputValueChanged(iv$previous, iv$current) ||
    hasInputValueChanged(getInputValue("macrophytesAssesmentAttribute"), iv$current))
  {
    # do not update plot now to avoid override of buffer with displayed input values
    # when clearing modifications - do it only in chained river type update
    iv.attr = macrophytesUpdateAssesmentAttributeValue(res,
      assesment.attribute=iv$current, update.plot=FALSE)
  }
  # chain update river type
  macrophytesUpdateAssesmentRiverType(res,
    update.plot=iv.attr[["update.plot.pending"]])
  NULL
})
#' Update assesment attribiute value input based on the API buffer and
#' results object
macrophytesUpdateAssesmentAttributeValue <- function(res, assesment.attribute=NULL,
  update.plot=NULL)
isolate({
  # print("macrophytesUpdateAssesmentAttributeValue")
  if (is.null(assesment.attribute))
    assesment.attribute = getInputValue("macrophytesAssesmentAttribute")
  inputName = "macrophytesAssesmentAttributeValue"
  inputValue = getInputValue(inputName)
  if (!is.null(res) && !isInputValueMissing(getInputValue("macrophytesObservationCode")) &&
      !isInputValueMissing(assesment.attribute))
  {
    newValue = macrophytesAPI$getObservationAttributeSpeciesValue(res,
      getInputValue("macrophytesObservationCode"),
      assesment.attribute)
    # OPT: change `min`/`max` and `step` kwargs here dep. on the
    #      `assesment.attribute` value
    iv = updateNumericInputWrapper(inputName, newValue)
    # print(iv)
    # There is observeEvent on the assesment attribute value
    if.update.plot = ifUpdatePlotDespiteObserveEvent(iv$previous, iv$current,
      update.now=update.plot)
    iv[["update.plot.pending"]] = if.update.plot$pending
    if (if.update.plot$now)
      macrophytesUpdateAssesmentPlots(getInputValue("macrophytesAssesmentPlotChoice"))

  } else {
    iv = list(previous=inputValue, current=inputValue, update.plot.pending=FALSE)
  }
  iv
})
#' Update river type name input based on the the API buffer and results object
macrophytesUpdateAssesmentRiverType <- function(res, update.plot=NULL) isolate({
  # print("macrophytesUpdateAssesmentRiverType")
  inputName = "macrophytesAssesmentRiverType"
  inputValue = getInputValue(inputName)
  if (!is.null(res) && !isInputValueMissing(getInputValue("macrophytesObservationCode")) &&
      !isInputValueMissing(getInputValue("macrophytesAssesmentPlotChoice")))
  {
    observationCode = getInputValue("macrophytesObservationCode")
    assChoice = getInputValue("macrophytesAssesmentPlotChoice")
    selected = NULL
    if (macrophytesIsAssesmentPlotPrelim(assChoice))
      selected = macrophytesAPI$getObservationRiverType(res, observationCode)
    else if (macrophytesIsAssesmentPlotFinal(assChoice))
      selected = macrophytesAPI$getObservationRiverTypeFinal(res, observationCode)
    iv = updateSelectInputWrapper(inputName, macrophytesAPI$getRiverTypes(res),
      selected=selected)
    # print(iv)
    # There is observeEvent on the assesment river type value
    if.update.plot = ifUpdatePlotDespiteObserveEvent(iv$previous, iv$current,
      update.now=update.plot)
    iv[["update.plot.pending"]] = if.update.plot$pending
    if (if.update.plot$now)
      macrophytesUpdateAssesmentPlots(getInputValue("macrophytesAssesmentPlotChoice"))
  } else {
    iv = list(previous=inputValue, current=inputValue, update.plot.pending=FALSE)
  }
  iv
})
ifUpdatePlotDespiteObserveEvent <- function(input.previous, input.current, update.now=NULL) {
  # Use if there is observeEvent on the input change that wuold normally trigger plot
  # update, but you want to force update in case the value hasn't changed
  update.pending = FALSE
  if(!hasInputValueChanged(input.previous, input.current)) {
    if (is.null(update.now)) update.now = TRUE
    else if (!update.now) update.pending = TRUE
  } else if (is.null(update.now)) update.now = FALSE
  list(now=update.now, pending=update.pending)
}

## interactive modifications: apply
##
macrophytesTypificationModifyAttributeValue <- function(res) {
  if (!is.null(res) &&
      !isInputValueMissing(getInputValue("macrophytesObservationCode")) &&
      !isInputValueMissing(getInputValue("macrophytesTypificationAttribute")) &&
      !isInputValueMissing(getInputValue("macrophytesTypificationAttributeValue")))
  {
    macrophytesAPI$setObservationAttributeTypeValue(
      res, getInputValue("macrophytesObservationCode"),
      getInputValue("macrophytesTypificationAttribute"),
      getInputValue("macrophytesTypificationAttributeValue")
    )
  }
}
macrophytesAssesmentModifyAttributeValue <- function(res) {
  # print("macrophytesAssesmentModifyAttributeValue")
  # for (inputName in c("macrophytesObservationCode", "macrophytesAssesmentAttribute", "macrophytesAssesmentAttributeValue")) {
  #   iv = getInputValue(inputName)
  #   print(paste("    Is", inputName, "missing?", isInputValueMissing(iv), "value =", iv))
  # }
  if (!is.null(res) &&
      !isInputValueMissing(getInputValue("macrophytesObservationCode")) &&
      !isInputValueMissing(getInputValue("macrophytesAssesmentAttribute")) &&
      !isInputValueMissing(getInputValue("macrophytesAssesmentAttributeValue")))
  {
    macrophytesAPI$setObservationAttributeSpeciesValue(
      res, getInputValue("macrophytesObservationCode"),
      getInputValue("macrophytesAssesmentAttribute"),
      getInputValue("macrophytesAssesmentAttributeValue")
    )
  }
  # chained update on river type here (because plot wrapper calls
  # "...ModifyAttributeValue" only)
  return(macrophytesAssesmentModifyRiverType(res))
}
macrophytesAssesmentModifyRiverType <- function(res) {
  # print("macrophytesAssesmentModifyRiverType")
  if (!is.null(res) &&
      !isInputValueMissing(getInputValue("macrophytesObservationCode")) &&
      !isInputValueMissing(getInputValue("macrophytesAssesmentRiverType")))
  {
    observationCode = getInputValue("macrophytesObservationCode")
    assChoice = getInputValue("macrophytesAssesmentPlotChoice")
    if (macrophytesIsAssesmentPlotPrelim(assChoice))
        macrophytesAPI$setObservationRiverType(
          res, getInputValue("macrophytesObservationCode"),
          getInputValue("macrophytesAssesmentRiverType"))
    else if (macrophytesIsAssesmentPlotFinal(assChoice))
        macrophytesAPI$setObservationRiverTypeFinal(
          res, getInputValue("macrophytesObservationCode"),
          getInputValue("macrophytesAssesmentRiverType"))
  }
}


macrophytesPreparePlotArgs <- function(modifyAttrFun, ...) {
  # first, modify attributes for the interactive plot
  modifyAttrFun(macrophytes.checkAndEvalRes)
  list(
    getInputValue("macrophytesObservationCode"),
    macrophytes.checkAndEvalRes,
    sampleSize = as.numeric(getInputValue("macrophytesNSamples")),
    dictionaryLanguage = getInputValue("macrophytesLanguage"),
    ...)
}
macrophytesPlot <- function(uiPrefix, apiFunName, ...) {
  alertOutputName = paste0(uiPrefix,"MessageOutput")
  plotOutputName = paste0(uiPrefix,"Plot")
  modifyAttrFun = base::get(paste0(uiPrefix,"ModifyAttributeValue"))
  plotFun = macrophytesAPI[[apiFunName]]
  # clean any previous status messages
  output[[alertOutputName]] <- renderUI(tags$div())
  # render the plot (prevent accidental dependencies)
  args = macrophytesPreparePlotArgs(modifyAttrFun, ...)
  # DEBUG
  # print("macrophytesPlot")
  # print(plotOutputName)
  # print(apiFunName)
  # print(args)
  # cat("\n\n")
  # if (!isInputValueMissing(getInputValue("macrophytesTypificationAttribute")))
  #   print(macrophytesAPI$getObservationAttributeTypeValue(
  #       macrophytes.checkAndEvalRes, getInputValue("macrophytesObservationCode"),
  #       getInputValue("macrophytesTypificationAttribute")))
  # if (!isInputValueMissing(getInputValue("macrophytesAssesmentAttribute")))
  #   print(macrophytesAPI$getObservationAttributeSpeciesValue(
  #       macrophytes.checkAndEvalRes, getInputValue("macrophytesObservationCode"),
  #       getInputValue("macrophytesAssesmentAttribute")))
  # cat("\n\n")
  output[[plotOutputName]] <- renderPlot({
    resWrap <- do.call(plotFun, args)
    # DEBUG print call log
    # output[[alertOutputName]] <- renderUI(
    #   tagInfoMsg(info.msg="DEBUG",
    #     logFileURL=getFileURL(prepLogFile(alertOutputName, resWrap)))
    # )
    if (outputLogger$hasError(resWrap)) {
      plotMsg(outputLogger$getError(resWrap))
      # OPT, put error box instead of a dummy plot
      # output[[alertOutputName]] <- renderUI(
      #   tagErrorMsg(err.msg=uiText$plot.error,
      #     logFileURL=getFileURL(prepLogFile(alertOutputName, resWrap)))
      # )
    }
  })
}


macrophytesIsTypificationPlotTypes <- function(typChoice)
  typChoice==uiText$types
macrophytesIsTypificationPlotGrowthForms <- function(typChoice)
  typChoice==uiText$growth.habit.group
macrophytesUpdateTypificationPlots <- function(typChoice) isolate({
  if (macrophytesIsTypificationPlotTypes(typChoice))
    macrophytesPlot("macrophytesTypification", "plotProbableTypes")
  else if (macrophytesIsTypificationPlotGrowthForms(typChoice))
    macrophytesPlot("macrophytesTypification", "plotProbableGrowthForms")
})
macrophytesIsAssesmentPlotPrelim <- function(assChoice) assChoice==uiText$orig
macrophytesIsAssesmentPlotFinal <- function(assChoice) assChoice==uiText$plaus
macrophytesUpdateAssesmentPlots <- function(assChoice) isolate({
  # print("macrophytesUpdateAssesmentPlots")
  if (macrophytesIsAssesmentPlotPrelim(assChoice))
    macrophytesPlot("macrophytesAssesment", "plotAssesmentOriginal",
      with.attrib=getInputValue("macrophytesIfAssesmentPlotWithAttrib"))
  else if (macrophytesIsAssesmentPlotFinal(assChoice))
    macrophytesPlot("macrophytesAssesment", "plotAssesmentPlausible",
      with.attrib=getInputValue("macrophytesIfAssesmentPlotWithAttrib"))
})
macrophytesUpdatePlots <- function(typChoice = uiText$types,
  assChoice = uiText$orig)
isolate({
  # print("macrophytesUpdatePlots")
  macrophytesUpdateTypificationPlots(typChoice)
  macrophytesUpdateAssesmentPlots(assChoice)
  NULL
})
# generate dummies at server start
macrophytesUpdatePlots()
macrophytesUpdatePlotsFromInputs <- function() {
  return(macrophytesUpdatePlots(getInputValue("macrophytesTypificationPlotChoice"),
      getInputValue("macrophytesAssesmentPlotChoice")))
}


## auto-update plots or attributes values whenever choices in UI change
##
observeEvent(input$macrophytesTypificationPlotChoice, isolate({
  # print("observeEvent: macrophytesTypificationPlotChoice")
  macrophytesUpdateTypificationPlots(getInputValue("macrophytesTypificationPlotChoice"))
}))
observeEvent(input$macrophytesAssesmentPlotChoice, isolate({
    # print("observeEvent: macrophytesAssesmentPlotChoice")
    # important: update possibly different river type input _before_ re-plotting
    macrophytesUpdateAssesmentRiverType(macrophytes.checkAndEvalRes)
    macrophytesUpdateAssesmentPlots(getInputValue("macrophytesAssesmentPlotChoice"))
}))
observeEvent(input$macrophytesObservationCode, isolate({
  # print("observeEvent: macrophytesObservationCode")
  macrophytesUpdateModificationAttributes(macrophytes.checkAndEvalRes)
}))
observeEvent(input$macrophytesTypificationAttribute, isolate({
  # print("observeEvent: macrophytesTypificationAttribute")
  macrophytesUpdateTypificationAttributeValue(macrophytes.checkAndEvalRes)
}))
observeEvent(input$macrophytesTypificationAttributeValue, isolate({
  # print("observeEvent: macrophytesTypificationAttributeValue")
  macrophytesUpdateTypificationPlots(getInputValue("macrophytesTypificationPlotChoice"))
}))
observeEvent(input$macrophytesAssesmentAttribute, isolate({
  # print("observeEvent: macrophytesAssesmentAttribute")
  macrophytesUpdateAssesmentAttributeValue(macrophytes.checkAndEvalRes)
}))
observeEvent(input$macrophytesAssesmentAttributeValue, isolate({
  # print("observeEvent: macrophytesAssesmentAttributeValue")
  macrophytesUpdateAssesmentPlots(getInputValue("macrophytesAssesmentPlotChoice"))
}))
observeEvent(input$macrophytesAssesmentRiverType, isolate({
  # print("observeEvent: macrophytesAssesmentRiverType")
  macrophytesUpdateAssesmentPlots(getInputValue("macrophytesAssesmentPlotChoice"))
}))
observeEvent(input$macrophytesIfAssesmentPlotWithAttrib, isolate({
  # print("observeEvent: macrophytesIfAssesmentPlotWithAttrib")
  macrophytesUpdateAssesmentPlots(getInputValue("macrophytesAssesmentPlotChoice"))
}))

# reset interactive plot modifications
macrophytesResetModifications <- function(res) {
    # print("macrophytesResetModifications")
    # since observation code stays same, explicitly clear the mod. buffer,
    # _before_ updating the modification attributes based on computed results
    macrophytesAPI$modBuffer$clear()
    # reset inputs - buffer is empty so only data in cached results object
    # will be used
    macrophytesUpdateModificationAttributes(res)
}
observeEvent({ input$macrophytesTypificationClearModifications }, isolate({
    # print("observeEvent: macrophytesTypificationClearModifications")
    macrophytesResetModifications(macrophytes.checkAndEvalRes)
}))
observeEvent({ input$macrophytesAssesmentClearModifications }, isolate({
    # print("observeEvent: macrophytesAssesmentClearModifications")
    macrophytesResetModifications(macrophytes.checkAndEvalRes)
}))

# Reset outputs, modifications and plots directly on language change
observeEvent(input$macrophytesLanguage, isolate({
    # print("observeEvent: macrophytesLanguage")
    macrophytesActionOutputsReset()
    stopifnot(is.null(macrophytes.checkAndEvalRes))
    macrophytesResetModifications(macrophytes.checkAndEvalRes)
    macrophytesUpdatePlots()
}))
#### END: interactive plots
