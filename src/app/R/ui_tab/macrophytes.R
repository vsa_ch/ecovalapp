# ui_tab/macrophytes.R
#
# UI tab: Macrophytes
#

# sourced via global.R:
#
# source("R/uiText.R")
# source("R/ecoval_api/commons.R")
# source("R/ecoval_api/macrophytes.R")


macrophytesTabPanel <- tabPanel(uiText$macrophytes,

  titlePanel(uiText$macrophytes.title),
  fluidRow(
    column(4,
      wellPanel(

        tags$h3(uiText$definitions),
        selectInput("macrophytesLanguage",
          paste0(uiText$language.encoding,":"),
          commonsAPI$dictionaryLanguages$choices,
          selected=commonsAPI$dictionaryLanguages$default),
        selectInput("macrophytesSamplingProtocol",
          paste0(uiText$sampling.protocol,":"),
          macrophytesAPI$samplingProtocols$choices,
          selected=macrophytesAPI$samplingProtocols$default),
        numericInput("macrophytesNSamples",
          label = paste0(uiText$samples.for.uncertainty,":"),
          value = macrophytesAPI$defaultSampleSize, min=0, step=100),


        tags$h3(uiText$input.data),
        directoryInput('macrophytesInputDataDir',
          btn.label = uiText$dir.data),
        directoryInput('macrophytesInputImgDir',
          btn.label = uiText$dir.images),
        div(class="narrow",
          textInput("macrophytesInputSeparator",
            label = paste0(uiText$separator," [",uiText$tab,"]:"),
            value = "")
        ),

        tags$h4(uiText$filenames),
        selectizeInput(inputId = "macrophytesSitesFileName",
            label = paste0(uiText$sites.data,"*:"),
            choices = NULL),
        selectizeInput(inputId = "macrophytesSpeciesFileName",
            label = paste0(uiText$species.data,":"),
            choices = NULL),
        selectizeInput(inputId = "macrophytesPlausibilityFileName",
            label = paste0(uiText$plausibilised.river.types,":"),
            choices = NULL),


        tags$h3(uiText$output.data),
        directoryInput('macrophytesOutputDir',
          btn.label = uiText$dir.output),
        div(class="narrow",
          textInput("macrophytesOutputSeparator",
            label = paste0(uiText$separator," [",uiText$tab,"]:"),
            value = "")
        ),

        tags$h4(uiText$filenames),
        textInput("macrophytesNumResFileName",
          label = paste0(uiText$numeric.results," [",macrophytesAPI$defaultFileExtensions$output.file.numeric,"]:"),
          value = uiText$filename.res.num),
        textInput("macrophytesDocFileName",
          label = paste0(uiText$documentation, " [",macrophytesAPI$defaultFileExtensions$output.file.doc,"]:"),
          value = uiText$filename.res.doc),
        textInput("macrophytesUsedTaxaFileName",
          label = paste0(uiText$used.taxa, " [",macrophytesAPI$defaultFileExtensions$output.file.taxa.used,"]:"),
          value = uiText$filename.used.taxa),
        textInput("macrophytesDiscardedTaxaFileName",
          label = paste0(uiText$discarded.taxa, " [",macrophytesAPI$defaultFileExtensions$output.file.taxa.removed,"]:"),
          value = uiText$filename.discarded.taxa),
        textInput("macrophytesQualityChecksFileName",
          label = paste0(uiText$quality.checks, " [",macrophytesAPI$defaultFileExtensions$output.file.quality.checks,"]:"),
          value = uiText$filename.quality.checks)

      ) # wellPanel
    ), # column



    column(8,
      #shinysky::busyIndicator(text=uiText$please.wait, wait=2000),
      tags$h3(uiText$evaluation_s_),
      checkboxInput("macrophytesIfWriteDoc",
        label = paste(uiText$incl, uiText$sites.documentation,
            paste0("[",macrophytesAPI$defaultFileExtensions$output.file.doc,"]")
          ),
        value = FALSE),
      fluidRow(
        column(3,
          shiny::actionButton("macrophytesCheckFilesAction",
            label = uiText$check.files),
          shinyjs::hidden(tags$div(id="macrophytesCheckFilesRunning",
              class="alert alert-info processing",
              tags$p(paste0(uiText$analysis.running,"...")))),
          htmlOutput("macrophytesCheckFilesOutput")
        ),
        column(3,
          shiny::actionButton("macrophytesComputeTypificationAction",
            label = uiText$compute.typification),
          shinyjs::hidden(tags$div(id="macrophytesComputeTypificationRunning",
              class="alert alert-info processing",
              tags$p(paste0(uiText$analysis.running,"...")))),
          htmlOutput("macrophytesComputeTypificationOutput")
        ),
        column(3,
          shiny::actionButton("macrophytesEvaluatePrelimAction",
            label = paste(uiText$assesment,uiText$prelim)),
          shinyjs::hidden(tags$div(id="macrophytesEvaluatePrelimRunning",
              class="alert alert-info processing",
              tags$p(paste0(uiText$analysis.running,"...")))),
          htmlOutput("macrophytesEvaluatePrelimOutput")
        ),
        column(3,
          shiny::actionButton("macrophytesEvaluatePlausAction",
            label = paste(uiText$assesment,uiText$plaus)),
          shinyjs::hidden(tags$div(id="macrophytesEvaluatePlausRunning",
              class="alert alert-info processing",
              tags$p(paste0(uiText$analysis.running,"...")))),
          htmlOutput("macrophytesEvaluatePlausOutput")
        )
      ),

      tags$h3(uiText$show.results),
      fluidRow(
        column(2,
          htmlOutput("macrophytesNumResOutput")
        ),
        column(3,
          htmlOutput("macrophytesUsedTaxaOutput")
        ),
        column(3,
          htmlOutput("macrophytesDiscardedTaxaOutput")
        ),
        column(2,
          htmlOutput("macrophytesQualityChecksOutput")
        ),
        column(2,
          htmlOutput("macrophytesDocumentationOutput")
        )
      ),


      # Note: this looks confusing
      # fluidRow(
      #   column(9,
      #     tags$h3(uiText$interactive.plots)
      #   ),
      #   column(3,
      #     selectizeInput(inputId = "macrophytesObservationCode",
      #       label = paste0(uiText$observation.code,":"),
      #       choices = NULL)
      #   )
      # ),
      tags$div( # <div> needed to limit overlay
        tags$h3(uiText$interactive.plots),
        tags$div(class="overlayRight",
          selectizeInput(inputId = "macrophytesObservationCode",
            label = paste0(uiText$observation.code,":"),
            choices = NULL)
        )
      ),

      tabsetPanel(
        tabPanel(uiText$typification,

          fluidRow(
            column(1,
              shiny::actionButton("macrophytesTypificationClearModifications",
                label = "", icon = icon("undo"),
                style='max-width: 30px; min-width: 30px;')
            ),
            column(3,
              selectizeInput(inputId = "macrophytesTypificationAttribute",
                label = paste0(uiText$attribute,":"),
                choices = NULL)
            ),
            column(2,
              numericInput("macrophytesTypificationAttributeValue",
                label = "= ", #paste0(uiText$level,":"),
                0, min=0, step=0.01),
              # report here issues w/ attrib. or value
              htmlOutput("macrophytesTypificationAttributeValueOutput")
            )
          ),

          # plot type
          radioButtons("macrophytesTypificationPlotChoice", "",
            choiceNames = list(uiText$types, uiText$growth.habit.group),
            choiceValues = list(uiText$types, uiText$growth.habit.group),
            inline = TRUE),

          htmlOutput("macrophytesTypificationMessageOutput"),
          # NOTE: wrapper div height calculated in ecoval.css as 100vh minus
          #       absolute height of content above plot in px
          #       on Win10 might not work in R Studio Browser, and in IE11
          #       works with "id" atrribute, but not with "class"
          tags$div(id="interactive-plot-macrophytes-1",
            plotOutput("macrophytesTypificationPlot", height="100%")
          )
        ),

        tabPanel(uiText$assesment,

          fluidRow(
            column(1,
              shiny::actionButton("macrophytesAssesmentClearModifications",
                label = "", icon = icon("undo"),
                style='max-width: 30px; min-width: 30px;')
            ),
            column(3,
              selectizeInput(inputId = "macrophytesAssesmentAttribute",
                label = paste0(uiText$attribute,":"),
                choices = NULL)
            ),
            column(2,
              numericInput("macrophytesAssesmentAttributeValue",
                label = "= ", #paste0(uiText$level,":"),
                0, min=0, step=0.01),
              # report here issues w/ attrib. or value
              htmlOutput("macrophytesAssesmentAttributeValueOutput")
            ),
            tags$div(class="absRight",
            # column(2, offset=2,
              selectizeInput(inputId = "macrophytesAssesmentRiverType",
                label = paste0(uiText$river.type,":"),
                choices = NULL)
            )
          ),

          # plot type
          fluidRow(
            column(6,
              radioButtons("macrophytesAssesmentPlotChoice", "",
                choiceNames = list(uiText$orig, uiText$plaus),
                choiceValues = list(uiText$orig, uiText$plaus),
                inline = TRUE)
            ),
            tags$div(class="absRight",
            # column(2, offset=2,
              checkboxInput("macrophytesIfAssesmentPlotWithAttrib",
                label = uiText$show.attributes,
                value = FALSE)
            )
          ),
          htmlOutput("macrophytesAssesmentMessageOutput"),
          # NOTE: wrapper div height calculated in ecoval.css as 100vh minus
          #       absolute height of content above plot in px
          #       on Win10 might not work in R Studio Browser, and in IE11
          #       works with "id" atrribute, but not with "class"
          tags$div(id="interactive-plot-macrophytes-2",
            plotOutput("macrophytesAssesmentPlot", height="100%")
          )
        )
      )
    ) # column
  ) # fluidRow

) # tabPanel
