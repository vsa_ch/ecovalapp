# ui_tab/home.R
#
# UI tab: Home
#

# sourced via global.R:
#
# source("R/uiText.R")

homeTabPanel <- tabPanel(uiText$navbar.home,

    fluidRow(
        column(9, offset = 0,
            titlePanel(uiText$home.title),
            tags$p(tags$strong(uiText$home.version.info))
        ),
        column(3, offset = 0,
        tags$br(),
        selectizeInput(inputId = "interfaceLanguage",
            label = tags$strong(paste0(uiText$interface.language,":")),
            selected = app.lang,  # Beware: must come before "choices" kwarg!
            choices = c(
                "Deutsch" = "de",
                "Francais" = "fr",
                "English" = "en")
            )
        )
    ),

    tags$h3(paste0(uiText$purpose,":")),
    tags$p(tags$strong(uiText$purpose.text)),
    tags$p(uiText$current.modules.lakes),
    tags$ul(
        tags$li(uiText$module.lakeshoremorph)
    ),
    tags$p(uiText$current.modules.rivers),
    tags$ul(
        tags$li(uiText$module.macrophytes),
        tags$li(uiText$module.morphology)
    ),

    tags$h3(paste0(uiText$references,":")),
    tags$ul(
        tags$li(tags$a("http://www.modul-stufen-konzept.ch"))
    ),

    tags$h3(paste0(uiText$contact.person,":")),
    tags$ol(
        tags$li("Christiane Ilg <christiane.ilg@vsa.ch>"),
        tags$li("Peter Reichert <peter.reichert@eawag.ch>"),
        tags$li("Nele Schuwirth <nele.schuwirth@eawag.ch>")
    ),

    tags$div(id="footer",
        tags$p(
            "Copyright (C) 2017-2022",
            tags$a(href = "https://sis.id.ethz.ch", "SIS ID ETH Zurich"),
            ", ",
            tags$a(href = "https://www.eawag.ch/en/department/siam/", "SIAM Eawag")
        )
    )

)
