# Subject to the BSD 3-Clause License
#
# Copyright (c) 2018, W. Lee Pang, PhD
# All rights reserved.
#
# Source: https://github.com/wleepang/shiny-directory-input/

#' @name choose.dir
#'
#' @title Choose a Folder Interactively
#'
#' Display an OS-native folder selection dialog under Mac OS X, Linux GTK+ or
#' Windows.
#'
#' @param default which folder to show initially
#' @param caption the caption on the selection dialog
#'
#' @details
#' Uses an Apple Script, Zenity or Windows Batch script to display an OS-native
#' folder selection dialog.
#'
#' For Apple Script, with \code{default = NA}, the initial folder selection
#' is determined by default behavior of the "choose folder" script. Otherwise,
#' paths are expanded with \code{\link{path.expand}}.
#'
#' For Linux, with \code{default = NA}, the initial folder selection is
#' determined by defaul behavior of the zenity script.
#'
#' The new windows batch script allows both initial folder and caption to be set.
#' In the old batch script for Windows the initial folder is always ignored.
#'
#' @return
#' A length one character vector, character NA if 'Cancel' was selected.
#'
#' @export
#'
choose.dir = function(default = NA, caption = NA) {
  if (Sys.info()['sysname'] == 'Darwin') {
    return(choose.dir.darwin(default = default, caption = caption))
  } else if (Sys.info()['sysname'] == 'Linux') {
    return(choose.dir.linux(default = default, caption = caption))
  } else if (Sys.info()['sysname'] == 'Windows') {
    # Use batch script to circumvent issue w/ `choose.dir`/`tcltk::tk_choose.dir`
    # window popping out unnoticed in the back of the current window
    return(choose.dir.windows(default = default, caption = caption))
  }
  return(paste("Error: don't know how to show a folder dialog in", Sys.info()['sysname']) )
}

#' @name choose.dir.darwin
#'
#' @title The apple version of the choose folder
#'
#' @seealso \code{\link{choose.dir}}
#'
#' @return
#' A length one character vector, character NA if 'Cancel' was selected.
#'
choose.dir.darwin <- function(default = NA, caption = NA) {
  command = 'osascript'
  args = '-e "POSIX path of (choose folder{{prompt}}{{default}})"'

  if (!is.null(caption) && !is.na(caption) && nzchar(caption)) {
    prompt = sprintf(' with prompt \\"%s\\"', caption)
  } else {
    prompt = ''
  }
  args = sub('{{prompt}}', prompt, args, fixed = T)

  if (!is.null(default) && !is.na(default) && nzchar(default)) {
    default = sprintf(' default location \\"%s\\"', path.expand(default))
  } else {
    default = ''
  }
  args = sub('{{default}}', default, args, fixed = T)

  suppressWarnings({
    path = system2(command, args = args, stderr = TRUE)
  })
  if (!is.null(attr(path, 'status')) && attr(path, 'status')) {
    # user canceled
    path = NA
  } else {
    # cut any extra output lines, like "Class FIFinderSyncExtensionHost ..."
    path = tail(path, n=1)
  }

  return(path)
}


#' @name choose.dir.linux
#'
#' @title The linux version of the choose folder
#'
#' @seealso \code{\link{choose.dir}}
#'
#' @return
#' A length one character vector, character NA if 'Cancel' was selected.
#'
choose.dir.linux <- function(default = NA, caption = NA) {
  command = 'zenity'
  args = '--file-selection --directory'

  if (!is.null(default) && !is.na(default) && nzchar(default)) {
    args = paste(args, sprintf('--filename="%s"', default))
  }

  if (!is.null(caption) && !is.na(caption) && nzchar(caption)) {
    args = paste(args, sprintf('--title="%s"', caption))
  }

  suppressWarnings({
    path = system2(command, args = args, stderr = TRUE)
  })

  #Return NA if user hits cancel
  if (!is.null(attr(path, 'status')) && attr(path, 'status')) {
    # user canceled
    return(NA)
  }

  #Error: Gtk-Message: GtkDialog mapped without a transient parent
  if(length(path) > 1){
    path = path[(length(path))]
  }

  return(path)
}

#' @name choose.dir.windows
#'
#' @title The windows version of the choose folder
#'
#' @seealso \code{\link{choose.dir}}
#'
#' @return
#' A length one character vector, character NA if 'Cancel' was selected.
#'
choose.dir.windows <- function(default = NA, caption = NA) {
  ## uses a powershell script gives a nicer interface
  ## and allows setting of the default directory and the caption
  whereisutils <- file.path('utils', 'newFolderDialog.ps1')
  command = 'powershell'
  args = paste('-NoProfile -ExecutionPolicy Bypass -File', normalizePath(whereisutils))
  if (!is.null(default) && !is.na(default) && nzchar(default)) {
    args = paste(args, sprintf('-default "%s"', normalizePath(default)))
  }

  if (!is.null(caption) && !is.na(caption) && nzchar(caption)) {
    args = paste(args, sprintf('-caption "%s"', caption))
  }

  suppressWarnings({
    path = system2(command, args = args, stdout = TRUE)
  })
}

.cutDirPath = function(x,maxchar=30) {  # cut file path for display in ui
  if ( length(x) != 1 ) return("")
  if ( nchar(x) <= maxchar ) return(x)
  s <- strsplit(x,split="/",fixed=TRUE)[[1]]
  n <- length(s)
  if ( n == 1 | nchar(s[n])>maxchar-3 ) return(paste0("...",substring(x,nchar(x)-maxchar+4)))
  cumchar <- rev(cumsum(rev(nchar(s)+1)))
  return(paste0("[...]/",paste(s[c(cumchar[1:(n-1)]<=maxchar-3,TRUE)],collapse="/")))
}

.chosenDirId = function(inputId) sprintf('%s__chosen_dir', inputId)
.displayedDirId = function(inputId) sprintf("%s__displayed_dir", inputId)

#' @name directoryInput
#'
#' @title Directory Selection Control
#'
#' @param inputId The \code{input} slot that will be used to access the value
#' @param label Display label for the control, or \code{NULL}/\code{""} for no label (default)
#' @param value Initial value.  Paths are expanded via \code{\link{path.expand}}.
#' @param btn.label Display label for the button; by default \code{\link{"..."}}.
#'
#' @details
#' This widget relies on \code{\link{choose.dir}} to present an interactive
#' dialog to users for selecting a directory on the local filesystem.  Therefore,
#' this widget is intended for shiny apps that are run locally - i.e. on the
#' same system that files/directories are to be accessed - and not from hosted
#' applications (e.g. from shinyapps.io).
#'
#' @return
#' A directory input control that can be added to a UI definition.
#'
#' @seealso
#' \code{\link{updateDirectoryInput}}, \code{\link{readDirectoryInput}}, \code{\link{choose.dir}}
#' @export
directoryInput = function(inputId, label = "", value = NULL, btn.label = "...") {
  if (!is.null(value) && !is.na(value)) {
    value = path.expand(value)
  }

  labelTag = if (nzchar(label))
    shiny:::`%AND%`(label, tags$label(`for`=inputId,label))
    else NULL

  tagList(
    singleton(
      tags$head(
        tags$script(src = 'js/directory_input_binding.js')
      )
    ),

    div(
      class = 'form-group directory-input-container',
      labelTag,
      div(
        # div(
        class = 'input-group shiny-input-container',
        style = 'display: block;',
        # div(class = 'input-group-addon', icon('folder-o')),
        tags$button(
          id = inputId,
          style = 'float: left;', # display: inline-block;
          class = 'btn btn-default input-group-addon directory-input',
          icon('folder-open'),
          HTML('&nbsp;'),
          btn.label
        ),
        # tags$input(
        #   id = .chosenDirId(inputId),
        #   value = value,
        #   type = 'text',
        #   class = 'form-control directory-input-chosen-dir',
        #   readonly = 'readonly'
        # )
        tags$input(
          id = .chosenDirId(inputId),
          value = value,
          type = 'text',
          style = 'display: none;',
          class = 'form-control directory-input-chosen-dir'
        ),
        tags$input(
          id = .displayedDirId(inputId),
          value = .cutDirPath(value),
          type = 'text',
          style = 'display: inline-block;',
          class = 'form-control directory-input-displayed-dir',
          readonly = 'readonly'
        )
        # ),
        # shiny::span(
        #   class = 'shiny-input-container',
        #   tags$button(
        #     id = inputId,
        #     class = 'btn btn-default directory-input',
        #     btn.label
        #   )
        # )
      )
    )
  )

}

#' @name updateDirectoryInput
#'
#' @title Change the value of a directoryInput on the client
#'
#' @param session The \code{session} object passed to function given to \code{shinyServer}.
#' @param inputId The id of the input object.
#' @param value A directory path to set
#' @param ... Additional arguments passed to \code{\link{choose.dir}}.  Only used
#'    if \code{value} is \code{NULL}.
#'
#' @details
#' Sends a message to the client, telling it to change the value of the input
#' object.  For \code{directoryInput} objects, this changes the value displayed
#' in the text-field and triggers a client-side change event.  A directory
#' selection dialog is not displayed.
#'
#'@export
updateDirectoryInput = function(session, inputId, value = NULL, ...) {
  if (is.null(value)) {
    value = choose.dir(...)
  }
  # session$sendInputMessage(inputId, list(chosen_dir = value))
  session$sendInputMessage(inputId, list(chosen_dir=value,
    displayed_dir=.cutDirPath(value)))
}

#' @name readDirectoryInput
#'
#' @title Read the value of a directoryInput
#'
#' @param session The \code{session} object passed to function given to \code{shinyServer}.
#' @param inputId The id of the input object
#'
#' @details
#' Reads the value of the text field associated with a \code{directoryInput}
#' object that stores the user selected directory path.
#'
#'@export
readDirectoryInput = function(session, inputId) {
  session$input[[.chosenDirId(inputId)]]
}
