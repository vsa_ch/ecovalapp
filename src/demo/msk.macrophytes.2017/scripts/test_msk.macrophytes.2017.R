# load package:
# =============

if ( !require("ecoval") ) { install.packages("ecoval"); library(ecoval) }


# specify directories and file names:
# ===================================

language <- c("Deutsch","Francais","English")

dir.inp          <- c("../input/DE",
                      "../input/FR",
                      "../input/EN")
dir.pict         <- "../input/pictures"
dir.out          <- c("../output/DE",
                      "../output/FR",
                      "../output/EN")

input.versions   <- c("max","doc","min")  # max: current inputs | doc: minimum inputs for complete documentation | min: minimum inputs for valuation

for ( input.version in paste("_",input.versions,sep="") )
{
  file.site        <- c(paste("Standortdaten",input.version,".txt",sep=""),
                        paste("DonneesSites",input.version,".txt",sep=""),
                        paste("DataSites",input.version,".txt",sep=""))
  file.species     <- c(paste("Artdaten",ifelse(input.version=="_min",input.version,""),".txt",sep=""),
                        paste("DonneesEspeces",ifelse(input.version=="_min",input.version,""),".txt",sep=""),
                        paste("DataSpecies",ifelse(input.version=="_min",input.version,""),".txt",sep=""))
  file.typeplaus   <- c("Flusstypen_Plaus.txt",
                        "TypesCoursdEau_Vrais.txt",
                        "Rivertypes_Plaus.txt")
  
  file.res1        <- c(paste("Standortdaten_Attribute_Bewertung_VorPlaus",input.version,".txt",sep=""),
                        paste("DonneesSites_Atributs_Evaluation_AventVrais",input.version,".txt",sep=""),
                        paste("DataSites_Attributes_Valuation_BeforePlaus",input.version,".txt",sep=""))
  file.res2        <- c(paste("Standortdaten_Attribute_Bewertung_Final",input.version,".txt",sep=""),
                        paste("DonneesSites_Atributs_Evaluation_Final",input.version,".txt",sep=""),
                        paste("DataSites_Attributes_Valuation_Final",input.version,".txt",sep=""))
  file.doc         <- c(paste("Standort_Dokumentation",input.version,".pdf",sep=""),
                        paste("Documentation_Sites",input.version,".pdf",sep=""),
                        paste("Documentation_Sites",input.version,".pdf",sep=""))
  file.taxaused    <- c("Taxa_verwendet.txt",
                        "Taxons_utilises.txt",
                        "Taxa_used.txt")
  file.taxaremoved <- c("Taxa_entfernt.txt",
                        "Taxons_supprimes.txt",
                        "Taxa_removed.txt")
  file.messages    <- c("Qualitaets_Check.txt",
                        "Controle_Qualite.txt",
                        "Quality_Check.txt")
  file.taxalist    <- c("Taxa_Liste.txt",
                        "Liste_Taxons.txt",
                        "Taxalist.txt")
  
  # run initial analysis:
  # =====================
  
  res1 <- list()
  for ( i in 1:length(language) )
  {
    # create output directory:
    
    if ( !dir.exists(dir.out[i]) ) dir.create(dir.out[i],recursive=TRUE)
    
    # run the analysis:
    
    cat("\nPerforming initial river macrophyte analysis in",language[i],"...\n")
    res1[[i]] <- msk.macrophytes.2017.read.compile.evaluate(file.site         = paste(dir.inp[i],file.site[i],sep="/"),
                                                            #pic.folder        = dir.pict,
                                                            file.species      = paste(dir.inp[i],file.species[i],sep="/"),
                                                            sampling.protocol = "v2018",
                                                            sampsize          = 10000,
                                                            file.res          = paste(dir.out[i],file.res1[i],sep="/"),
                                                            file.doc          = paste(dir.out[i],file.doc[i],sep="/"),
                                                            file.taxa.used    = paste(dir.out[i],file.taxaused[i],sep="/"),
                                                            file.taxa.removed = paste(dir.out[i],file.taxaremoved[i],sep="/"),
                                                            file.check.msg    = paste(dir.out[i],file.messages[i],sep="/"),
                                                            language          = language[i])

    # write taxa list:
    
    write.table(res1[[i]]$taxalist,paste(dir.out[i],file.taxalist[i],sep="/"),col.names=T,row.names=F,sep="\t",na="")
  }

  # run analysis with plausibilized river types:
  # ============================================
  
  res2 <- list()
  for ( i in 1:length(language) )
  {
    # create output directory:
    
    if ( !dir.exists(dir.out[i]) ) dir.create(dir.out[i],recursive=TRUE)
    
    # run the analysis:
    
    cat("\nPerforming river macrophyte analysis with plausibilized river types in",language[i],"...\n")
    res2[[i]] <- msk.macrophytes.2017.read.compile.evaluate(file.site         = paste(dir.inp[i],file.site[i],sep="/"),
                                                            pic.folder        = dir.pict,
                                                            file.species      = paste(dir.inp[i],file.species[i],sep="/"),
                                                            file.typeplaus    = paste(dir.inp[i],file.typeplaus[i],sep="/"),
                                                            sampling.protocol = "v2018",
                                                            sampsize          = 10000,
                                                            file.res          = paste(dir.out[i],file.res2[i],sep="/"),
                                                            file.doc          = paste(dir.out[i],file.doc[i],sep="/"),
                                                            file.taxa.used    = paste(dir.out[i],file.taxaused[i],sep="/"),
                                                            file.taxa.removed = paste(dir.out[i],file.taxaremoved[i],sep="/"),
                                                            file.check.msg    = paste(dir.out[i],file.messages[i],sep="/"),
                                                            language          = language[i])
  }
}

# (Re-)produce results from output files:

for ( i in 1:length(file.res2) )
{
  valfun <- msk.macrophytes.2017.create(language=language[i])
  attrib <- read.table(paste(dir.out[i],file.res2[i],sep="/"),header=TRUE,sep="\t",stringsAsFactors=FALSE)
  val <- evaluate(valfun,attrib)
  plot(valfun,val[round((nrow(val)+1)/2),],two.lines=TRUE)
}







