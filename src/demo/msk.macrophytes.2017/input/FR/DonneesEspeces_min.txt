Code_Site	Date	NoEspece_SMG_FC	NomEspece_Latin	Recouvrement_abs	Frequence_rel_artificiel	Fiabilite_Determination	EtatPheno_Fleurs	EtatPheno_Fruits
200	16.08.2016	9300	Agrostis stolonifera L.	1		1		
200	21.08.2017	9300	Agrostis stolonifera L.	1		1		
200	16.08.2016	21900	Alisma plantago-aquatica L.	0.25		3	1	
200	16.08.2016	73200	Callitriche obtusangula Le Gall	1.5		4	1	1
200	16.08.2016	83000	Carex acutiformis Ehrh.	0.25		3		
200	16.08.2016	146200	Elodea canadensis Michx.	2		1		
200	21.08.2017	146200	Elodea canadensis Michx.	10		1		
200	16.08.2016	258000	Mentha aquatica L.	1		1	1	
200	21.08.2017	258000	Mentha aquatica L.	0.4		1	1	
200	16.08.2016	269900	Nasturtium officinale R. Br.	10		1	1	
200	21.08.2017	269900	Nasturtium officinale R. Br.	8		1		
200	16.08.2016	297900	Phalaris arundinacea L.	7.75		1	1	
200	21.08.2017	297900	Phalaris arundinacea L.	1		1		
200	16.08.2016	338500	Ranunculus fluitans Lam.	35		1	1	
200	21.08.2017	338500	Ranunculus fluitans Lam.	29		1		
200	16.08.2016	378200	Schoenoplectus lacustris (L.) Palla	1		1	1	
200	21.08.2017	378200	Schoenoplectus lacustris (L.) Palla	0.2		1	1	
200	16.08.2016	379700	Scirpus sylvaticus L.	2		1	1	
200	21.08.2017	382200	Scrophularia umbrosa Dumort.	0.4		4	1	
200	16.08.2016	432100	Typha angustifolia L.	2		3	1	1
200	21.08.2017	432100	Typha angustifolia L.	0.5		1	1	1
200	16.08.2016	432200	Typha latifolia L.	2		3	1	1
200	21.08.2017	432200	Typha latifolia L.	2		1		
200	21.08.2017	440500	Veronica anagallis-aquatica L.	1		3		
200	16.08.2016	441000	Veronica beccabunga L.	2.25		1		
200	21.08.2017	441000	Veronica beccabunga L.	0.2		1		
200	16.08.2016	10000516	Leptodictyum riparium (Hedw.) Warnst.	-999		2		
200	21.08.2017	10000516	Leptodictyum riparium (Hedw.) Warnst.	-999		2		
200	16.08.2016	10001401	Fontinalis antipyretica Hedw.	-999		1		
200	21.08.2017	10001401	Fontinalis antipyretica Hedw.	-999		1		
200	21.08.2017	30000001	Callitriche sp.	1		1		
200	16.08.2016	30000083	Carex 1	0.25		1		
200	21.08.2017	30000083	Carex 1	2		1		
200	21.08.2017	30000085	Carex 3	2		1		
200	16.08.2016	30000115	Sparganium 1	1		4		
200	21.08.2017	30000129		1		1	1	
200	16.08.2016	50000001	Bryophyta	1	30	1		
200	21.08.2017	50000001	Bryophyta	0.6	80	1		
200	16.08.2016	50000004	faedige Gruenalgen	1	30	1		
200	16.08.2016	60000001	Berula erecta (Huds.) Coville	1		1		
200	21.08.2017	60000001	Berula erecta (Huds.) Coville	0.5		1	1	
200	21.08.2017	60000012	Sparganium emersum Rehmann	0.2		3		
200	16.08.2016	60000013	Sparganium erectum L. s.l.	2		1	1	1
353	23.06.2016	10000535	Hygroamblystegium tenax (Hedw.) Jenn.	-999		2		
353	23.06.2016	10001401	Fontinalis antipyretica Hedw.	-999		2		
353	23.06.2016	10002124	Rhynchostegium riparioides (Hedw.) Cardot	-999		2		
353	23.06.2016	10003415	Fissidens crassipes Bruch & Schimp. subsp. crassipes	-999		2		
353	23.06.2016	50000001	Bryophyta	9	100	1		
353	23.06.2016	50000004	faedige Gruenalgen	1	100	1		
355	26.05.2016	10000516	Leptodictyum riparium (Hedw.) Warnst.	-999		2		
355	26.05.2016	10002124	Rhynchostegium riparioides (Hedw.) Cardot	-999		2		
355	26.05.2016	10003297	Cratoneuron filicinum (Hedw.) Spruce	-999		2		
355	26.05.2016	50000001	Bryophyta	2	50	1		
355	26.05.2016	500005126		-999		2		
463	04.07.2016	9300	Agrostis stolonifera L.	1		1		
463	04.07.2016	191700	Glyceria notata Chevall.	1		1	1	
463	04.07.2016	269900	Nasturtium officinale R. Br.	1		1	1	
463	04.07.2016	297900	Phalaris arundinacea L.	5		1	1	
463	04.07.2016	441000	Veronica beccabunga L.	2		1		
463	04.07.2016	10001401	Fontinalis antipyretica Hedw.	-999		2		
463	04.07.2016	10002124	Rhynchostegium riparioides (Hedw.) Cardot	-999		2		
463	04.07.2016	10003415	Fissidens crassipes Bruch & Schimp. subsp. crassipes	-999		2		
463	04.07.2016	50000001	Bryophyta	1	70	1		
463	04.07.2016	500005127		-999		2		
5640	18.07.2017	267800	Myriophyllum spicatum L.	2		1		
5640	18.07.2017	338500	Ranunculus fluitans Lam.	7.5		1		
5640	18.07.2017	10000516	Leptodictyum riparium (Hedw.) Warnst.	-999		2		
5640	18.07.2017	10001401	Fontinalis antipyretica Hedw.	-999		2		
5640	18.07.2017	10002124	Rhynchostegium riparioides (Hedw.) Cardot	-999		2		
5640	18.07.2017	10003415	Fissidens crassipes Bruch & Schimp. subsp. crassipes	-999		2		
5640	18.07.2017	50000001	Bryophyta	1	100	1		
5640	18.07.2017	50000004	faedige Gruenalgen	0.5	0	1		
5724	27.06.2016	9300	Agrostis stolonifera L.	1		1		
5724	27.06.2016	83000	Carex acutiformis Ehrh.	10		4	1	
5724	27.06.2016	191700	Glyceria notata Chevall.	2		1	1	
5724	27.06.2016	267900	Myriophyllum verticillatum L.	2		2		
5724	27.06.2016	269900	Nasturtium officinale R. Br.	3		1	1	
5724	27.06.2016	297900	Phalaris arundinacea L.	10		1	1	
5724	27.06.2016	440500	Veronica anagallis-aquatica L.	5		1	1	
5724	27.06.2016	50000004	faedige Gruenalgen	2	0	1		
5724	27.06.2016	60000013	Sparganium erectum L. s.l.	5		1	1	
5893	27.06.2016	269900	Nasturtium officinale R. Br.	0.2		3		
5893	27.06.2016	297900	Phalaris arundinacea L.	0.2		1	1	
5893	27.06.2016	10000516	Leptodictyum riparium (Hedw.) Warnst.	-999		2		
5893	27.06.2016	10001401	Fontinalis antipyretica Hedw.	-999		2		
5893	27.06.2016	10002124	Rhynchostegium riparioides (Hedw.) Cardot	-999		2		
5893	27.06.2016	50000001	Bryophyta	4	0	1		
5893	27.06.2016	50000004	faedige Gruenalgen	1	0	1		
5893	27.06.2016	500005127		-999		2		
9222	04.08.2017	60000013	Sparganium erectum L. s.l.	60	120	1	1	
9222	04.08.2017	297900	Phalaris arundinacea L.	5		1		
9222	04.08.2017	441000	Veronica beccabunga L.	1		1		
9222	04.08.2017	319200	Potamogeton natans L.	4		1		
9222	04.08.2017	60000012	Sparganium emersum Rehmann	5		1		
9186	17.08.2017	50000004	faedige Gruenalgen	10	20	1		
9186	17.08.2017	50000001	Bryophyta	0.1	100	1		
9186	17.08.2017	10001401	Fontinalis antipyretica Hedw.	0.1	100	1		
9186	17.08.2017	319300	Potamogeton nodosus Poir.	5		1		
9186	17.08.2017	319700	Potamogeton pectinatus L.	5		1		
9186	17.08.2017	60000013	Sparganium erectum L. s.l.	0.2		1		
9186	17.08.2017	30000001	Callitriche sp.	0.1		1		
9186	17.08.2017	378200	Schoenoplectus lacustris (L.) Palla	0.1		1		
9186	17.08.2017	269900	Nasturtium officinale R. Br.	0.1		1	1	
