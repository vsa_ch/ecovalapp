# load package:
# =============

if ( !require("ecoval") ) { install.packages("ecoval"); library(ecoval) }


# specify directories and file names:
# ===================================

language <- c("Deutsch","Francais","English")

dir.inp          <- c("../input/DE",
                      "../input/FR",
                      "../input/EN")
dir.out          <- c("../output/DE",
                      "../output/FR",
                      "../output/EN")

file.morphol     <- c("Oekomorphologiedaten.txt",
                      "DonneesEcomorphologie.txt",
                      "DataEcomorphology.txt")

file.res         <- c("Oekomorphologie_Bewertung.txt",
                      "Ecomorphologie_Evaluation.txt",
                      "Ecomorphology_Valuation.txt")
file.hier        <- c("Zielhierarchien.pdf",
                      "HierarchiesDesObjectifs.pdf",
                      "Objectiveshierarchies.pdf")
file.lines       <- c("Bewertungslinien.pdf",
                      "LignesdEvaluation.pdf",
                      "ValuationLines.pdf")


# run analysis:
# =============

for ( i in 1:length(language) )
{
  cat("\nPerforming lake shore morphology analysis in",language[i],"...\n")

  # create output directory:
  
  if ( !dir.exists(dir.out[i]) ) dir.create(dir.out[i],recursive=TRUE)
  
  # construct value function:
  
  lake.morphol <- lake.morphol.2016.create(language[i])
  
  # read attributes:
  
  attrib <- lake.morphol.2016.read.attrib(dir.inp[i],language=language[i])
  write.table(attrib,paste(dir.out[i],file.morphol[i],sep="/"),col.names=NA,row.names=TRUE,sep="\t",na="")
  
  # calculate values:
  
  val <- evaluate(lake.morphol,attrib)
  write.table(val,paste(dir.out[i],file.res[i],sep="/"),col.names=NA,row.names=TRUE,sep="\t",na="")
  
  # plot objectives hierarchies:
  
  pdf(paste(dir.out[i],file.hier[i],sep="/"),height=8,width=15)
  plot(lake.morphol,val,two.lines=TRUE,main=rownames(val),cex.nodes=0.8)
  dev.off()
  
  # plot valuation lines:
  
  pdf(paste(dir.out[i],file.lines[i],sep="/"),height=8,width=15)
  lake.morphol.2016.plot.val.spatial(val,lwd=3)
  dev.off()
}

# simple example:

val  <- evaluate(lake.morphol,data.frame(E01="E01.01",
                                         E02="E02.0303",
                                         B02="B02.01",
                                         B01="B01.01",
                                         C06="C06.01",
                                         C01="C01.04",
                                         C02="C02.01",
                                         C03="C03.02",
                                         C04="C04.01",
                                         C05="C05.01",
                                         D01="D01.0404",
                                         D02="D02.04"))
print(t(val))

