# Contributing

## Report Bugs

Report bugs at https://gitlab.com/vsa_ch/ecovalapp/-/issues.

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

## Setup Development Environment

1. Install pre-requisites:

   * [R](https://cran.r-project.org/)
   * [RStudio Desktop](https://rstudio.com/products/rstudio/download/),
   * [node.js](https://nodejs.org/en/)
   * On Windows [Git client with recommended Git Bash](https://gitforwindows.org/).

   On Windows, to make Git Bash see the `npm` command, open the
   `C:\Program Files\Git\etc\profile` file in a text editor, find `export PATH` and
   before line containing this put:

       # npm
       PATH="${PATH}:/c/Program Files/nodejs"

2. Setup development environment; in Bash shell run:

       $ git clone https://gitlab.com/vsa_ch/ecovalapp.git
       $ cd ecovalapp/src/app
       $ npm install

   To test the full Electron-based setup run:

       $ npm run start

   Otherwise, for the local development use the RStudio app. In the RStudio app, select
   "File > New Project... > Existing Directory", then navigate and choose the
   `"ecovalapp"` directory. Install the R dependencies:

   ```R
   > source("scripts/install_packages.R")
   > source("scripts/install_test_packages.R")
   ```
   In case the test dependencies fail to install you might need to install additional
   system packages (do read the error output); for instance:

      * on macOS:

            $ brew install libgit2

      * on Linux Debian, Ubuntu, etc:

            $ apt-get install libgit2-dev

   Finally, open either `src/app/ui.R` or `src/app/server.R` file and select the
   "Run App" button in the file editor's upper-right corner.

## Run Unit Tests

In a R Console, with the `src/app` directory set as an active directory, run:
```R
> shiny::runTests(filter = "testthat")
```

### (Re-)create snapshot files

For outputs files as well as for plots we regress on file snapshots created via
[`testthat::expect_snapshot_file`](https://testthat.r-lib.org/reference/expect_snapshot_file.html)
calls.

In case of newly added snapshots checks the snapshot files will be created in
`testthat/_snaps` subfolders, on a first test run; add these files to the repository.

In case the code generating files, for which snapshots already exists, has changed, the
tests will fail and new snapshot files will be created, with a `.new` suffix. Review
these files either manually or using the helper Shiny app and accept the new snapshots.
In a R Console, with the `src/app` directory set as an active directory, run:
```R
> testthat::snapshot_review()
```
for the helper Shiny snapshot review app, or, review new snapshot files manually and
accept by running:
```R
> testthat::snapshot_accept()
```

Note: The `DESCRIPTION` file and the `R/` folder name for sourced files are required
both for using snapshot checks (which require `Config/testthat/edition: 3`) and for
running snapshot review or acceptance (these expect a standard R package layout).

Note 2: PDF files are not suitable for snapshot file check due to included creation and
modification dates. Instead, we use a home-brewed PDF-specific snapshot check that
simply writes the expected file on the first run if not available and fails if the files
content differ, minus the dates. Reviewing for changes has to be done manually by
deletion and check of the expected file in a `testthat/_data` subfolder.

## Fix Bugs or Implement Features

Short step-by-step instructions for the co-development workflow. Steps marked with (*)
can be omitted, depending on the scale and goal of the changes.

1. (*) [Create a GitLab Issue](https://gitlab.com/vsa_ch/ecovalapp/-/issues/new)
   corresponding to your task/feature and from that's issue view press "Create merge
   request" dropdown menu; select as a target branch the `main` branch and, optionally,
   change the auto-generated name of the source branch. Afterwards, fetch the newly
   created source branch and switch to it:

       $ git fetch -a
       $ git switch NAME_OF_BRANCH

   Alternatively, instead of going via GitLab interface, directly with your Git client
   branch out from the `main` branch to a new task/feature-oriented branch:

       $ git checkout main
       $ git switch -c NAME_OF_BRANCH

   and, if needed for collaboration, setup branch to be tracked remotely:

       $ git push --set-upstream origin french_lang_checks

2. Introduce your work (in one or more commits) and after testing during each commit
   step in the EcoValApp using RStudio viewer, e.g.

       $ git add R/uiText.R
       $ git commit -m "updated translations"
       $ git add R/ecoval_api/macrophytes.R
       $ git commit -m "improve the msk.macrophytes plots"
       ...

3. (*) If done in a separate branch, merge with the `main` branch either by going
   over the merge request in a Web browser ("Mark as ready" > "Merge"), or directly,
   using Git client:

       $ git switch main
       $ git pull
       $ git merge --no-ff NAME_OF_BRANCH

4. Upload your changes to the server

       $ git push

5. (*) If done in a separate branch, after a successful merge and synchronization with
   the server, delete your local and remote branches:

       $ git branch -d NAME_OF_BRANCH
       $ git push origin --delete NAME_OF_BRANCH

## Update or install R package


1. Open a command line in the `src/app/R-Portable-Mac` or `src/app/R-Portable-Win`
   directory and run:

       $ bin/R

2. In the local R console:
   * to update all outdated dependencies run:
      ```R
      > source("../../../scripts/install_packages.R")
      ```
   * to update only a single outdated package, e.g. `"ecoval"`, run:
      ```R
      > pkg_name <- "ecoval"
      > cran_repo <- "https://stat.ethz.ch/CRAN/"
      > if (pkg_name %in% rownames(old.packages(repos = cran_repo))) install.packages(pkg_name, repos = cran_repo)
      ```
   * to install a new package run:
      ```R
      > pkg_name <- "..."
      > cran_repo <- "https://stat.ethz.ch/CRAN/"
      > install.packages(pkg_name, repos = cran_repo)
      ```
3. Quit the local R console:
   ```R
   > q(save = "no")
   ```

4. Add and commit the changes in the `src/app/R-Portable-Mac` or
   `src/app/R-Portable-Win` directory; push the commit.
5. Prepare a release (see below).

## Update R version

You need to update portable R versions for both Windows and macOS, both requiring
installations in the respective operating systems.

### Windows

1. [Download the newest `R-Portable_N.M.K.paf.exe` file from the R Portable SourceForge
   repository](https://sourceforge.net/projects/rportable/files/R-Portable/).
2. Run the `R-Portable_N.M.K.paf.exe` to install R-Portable locally in a temporary
   directory `{TEMP_DIR}` (e.g. in the `Downloads/` directory).
3. Delete `src/app/R-Portable-Win` directory from the repository.
4. Move `{TEMP_DIR}/R-Portable/App/R-Portable` directory to `src/app/` directory in the
   repository and rename it to `R-Portable-Win`.
5. Open a command line in the `src/app/R-Portable-Win` directory and install
   dependencies by running:

       $ bin/Rscript.exe ../../../scripts/install_packages.R

6. Test that the application works by running in the `src/app/` directory:

       $ npm run start

   and by checking printed out R version and by running manual tests for evaluation and
   plots.

7. Add and commit the new `src/app/R-Portable-Win` directory; push the commit.
8. Prepare a release (see below).

### macOS

 0. If applicable, uninstall the Homebrew R installation:

        $ brew uninstall r

 1. [Download macOS R file `R-N.M.K.pkg` from the official R downloads in a version
    equivalent to the Windows R Portable
    version](https://cran.r-project.org/bin/macosx/). Note: Windows R Portable version
    can be older then newest official macOS R version.
 2. Run `R-N.M.K.pkg` to install R in version matching the R Portable version.
 3. Delete `src/app/R-Portable-Mac` directory from the repository.
 4. Move `/Library/Frameworks/R.framework/Versions/N.M.K/Resources` directory to
    `src/app/` directory in the repository and rename it to `R-Portable-Mac`.
 5. Using Git show changes for `R-Portable-Mac/bin/R` script with respect to the commited
    version (from a command line in repo root `git diff src/app/R-Portable-Mac/bin/R`).
    You will see difference at the beginning regarding path variables `R_HOME_DIR`,
    `R_SHARE_DIR`, `R_INCLUDE_DIR`, `R_DOC_DIR` starting currently with
    `/Library/Frameworks/R.framework/Resources`. These variables are commented out in
    the previous commit and replaced with local paths, including a larger block in the
    beginning of the file for setting the `R_HOME_DIR` variable. Do apply same path
    variables changes to your current `R-Portable-Mac/bin/R` file, such that git does
    not show differences in these variables anymore.
 6. Fix dynamic libraries linking, in the repo root directory run:

        $ scripts/dylib_relink.sh

 7. Fix fonts symbolic links to use relative paths instead of absolute paths;
    in `src/app/R-Portable-Mac/fontconfig/fonts/conf.d` directory run:

        $ for f in *.conf; do ln -sf ../../fontconfig/conf.avail/${f} ${f}; done

 8. Remove macOS R by running:

        $ sudo rm -rf /Library/Frameworks/R.framework /Applications/R.app /usr/bin/R /usr/bin/Rscript

 9. Open a command line in the `src/app/R-Portable-Mac` directory and install
    dependencies by running:

        $ bin/R

    and then in the local R console:
       ```R
       > source("../../../scripts/install_packages.R")
       ```
    Note: running directly via `bin/Rscript` won't work - the binary is unfortunately
    broken when moved in the macOS.
10. Test that the application works by running in the `src/app/` directory:

        $ npm run start

    and by checking printed out R version and by running manual tests for evaluation and
    plots.

11. Add and commit the new `src/app/R-Portable-Mac` directory; push the commit.
12. Prepare a release (see below).

## Prepare a Release

 1. Make sure all your changes are committed and pushed.

 2. Create `release-N.M.K` branch, with next release number `N.M.K` (you will find the
    current release number e.g. in `src/app/package.json`):

        $ git switch -c `release-N.M.K`

    Increase:
    * `N` for major backward-incompatible releases,
    * `M` for minor releases with new features and bug fixes, and
    * `K` for patch releases with bug or documentation or dependencies fixes.

 3. Update `README.md` file, if needed.

 4. Update release version in `uiText.R`, `DESCRIPTION`, `package.json`, and
    `package-lock.json` files to `N.M.K-rc` version (`rc` stands for a release
    candidate).

    Note: release candidate users acceptance (`-rc` suffix) is optional, especially for
    patch updates.

    Commit the bump version files (replace `N.M.K-rc` with appropriate version number):

        $ git commit -m "bump version to N.M.K-rc" src/app/{uiText.R,DESCRIPTION,package.json,package-lock.json}

 5. Run the build script, both on Windows:

        $ scripts/build.sh win

    and on macOS:

        $ scripts/build.sh mac

    This will create both unpacked portable apps in `dist/N.M.K` folder as well as
    OS-specific installers (`.exe` or `.dmg` files respectively).

    Note: it's recommended to build packages on target platforms, i.e. `.exe` on Windows
    and `.dmg` on macOS, but if you want you can run both builds on macOS. To that end
    you need to install `mono` and `wine` e.g. through Homebrew package manager.

 6. Uninstall old version of `EcoValApp` from Windows and macOS, install new version and
    test the installation and app. If necessary, fix and repeat steps 5. and 6.

 7. Hand-out the release candidate binaries for users tests and acceptance. If needed,
    implement feedback, and repeat steps 5. to 7.; use separate bug fixes or feature
    branches as described in the previous section, but with the `release-N.M.K` branch
    as a target branch (instead of the `main` branch).

 8. After users acceptance, do steps 4. and 5. but update release version from  `N.M.K`
    to `N.M.K-rc`. Distribute Windows and macOS binaries.

 9. Create a new version Git tag:

        $ git tag -m "Release N.M.K" vN.M.K
        $ git push --tags

10. Merge the release branch into main branch with `--no-ff` flag and delete the release
    branch::

        $ git switch main
        $ git merge --no-ff release-N.M.K
        $ git push
        $ git push --delete origin release-N.M.K
        $ git branch --delete release-N.M.K
